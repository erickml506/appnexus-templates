(function () {
  var
    CACHEBUSTER = '${CACHEBUSTER}'

  var CREATIVE_ID = '${CREATIVE_ID}'

  var CREATIVE_WIDTH = '${CREATIVE_WIDTH}'

  var CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}'

  var MEDIA_URL = '${MEDIA_URL}'

  var CLICK_URL = '${CLICK_URL}'

  var URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}'

  var MEDIA_VIDEO_IMG_SHORT = '#{MEDIA_VIDEO_IMG_SHORT}'

  var MEDIA_VIDEO_IMG_BIG = '#{MEDIA_VIDEO_IMG_BIG}'

  var EXTENSION_TYPE = '#{EXTENSION_TYPE}'

  var IMG_CLOSE = '#{IMG_CLOSE}'

  var ICON_BUTTON_PLAY = '#{ICON_BUTTON_PLAY}'

  var ICON_BUTTON_PAUSE = '#{ICON_BUTTON_PAUSE}'

  var ICON_BUTTON_VOL = '#{ICON_BUTTON_VOL}'

  var ICON_BUTTON_VOL_MUTED = '#{ICON_BUTTON_VOL_MUTED}'

  const makeUrlRedirect = function (url, urlredir) {
    urlredir = url.split('clickenc=')[0] + 'clickenc=' + urlredir
    return urlredir
  }
  const getWidthOrHeight = function (size, wh) {
    return size.toLocaleLowerCase()
      .split('x')[wh == 'width' ? 0 : 1] + 'px'
  }
  const object_assign = function () {
    var obj = arguments[0]
    var field
    for (var index = 1; index < arguments.length; index++) {
      var args = arguments[index]
      for (field in args) {
        obj[field] = args[field]
      }
    }
    return obj
  }
  const setAnchorTo = function (divToappend, anchor_click) {
    divToappend.appendChild(anchor_click)
    anchor_click.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT), '_blank')
    })
  }

  const resetCounter = function () {
    clearInterval(interval)
    divCounterVideo.style.visibility = 'hidden'
    counter = 3
  }

  var parentBody = document.body.ownerDocument.defaultView.parent.document.body

  var win = document.body.ownerDocument.defaultView.parent

  var windowParentWidth = win.outerWidth

  var windowParentHeigth = win.innerHeight

  var windowParentHeigthOuter = win.outerHeight

  var divVideoSmallParent = document.createElement('div')

  var divVideoSmall = document.createElement('div')

  var divCounterVideo = document.createElement('div')

  var counter = 3

  var anchorClickVideoSmall = document.createElement('a')

  var divVideoBigParent = document.createElement('div')

  var divVideoBigFirstChild = document.createElement('div')

  var divVideoBigSecondChild = document.createElement('div')

  var divVideoBigThirdChild = document.createElement('div')

  var divVideoBigFourthChild = document.createElement('div')

  var divVideoBigFithChild = document.createElement('div')

  var divVideoBigContent = document.createElement('div')

  var anchorClickVideoBig = document.createElement('a')

  var divCloseParent = document.createElement('div')

  var imgClose = document.createElement('img')

  var divControlsParent = document.createElement('div')

  var divControlsFirstChild = document.createElement('div')

  var divControlPlay = document.createElement('div')

  var iconVideoPlay = document.createElement('a')

  var divControlVol = document.createElement('div')

  var volIcon = document.createElement('a')

  var countDownVideo = document.createElement('div')

  var countDownChild = document.createElement('div')

  parentBody.style.cssText += 'overflow-x: hidden !important'
 
  iconVideoPlay.id = 'iconVideoPlay'

  var interval,
    intervalCounter

  const closeVideoBig = function (e) {
    divVideoBigParent.style.top = '100%'//windowParentHeigth + 'px'
    imgClose.parentElement.style.visibility = 'hidden'
    divVideoBigContent.childNodes[2].currentTime = 0
    divVideoBigContent.childNodes[2].pause()
    parentBody.style.overflowY = ''
    clearInterval(intervalCounter)
  }
  const counterVideoText = function () {
    intervalCounter = setInterval(function () {
      var countAd = divVideoBigContent.childNodes[2].duration - divVideoBigContent.childNodes[2].currentTime
      countDownChild.innerText = 'El anuncio terminará en ' + Math.round(countAd) + 's'
    }, 1000)
  }
  const decreaseCounter = function () {
    divCounterVideo.innerText = '3'
    divCounterVideo.style.visibility = 'visible'
    interval = setInterval(function () {
      if (counter > 0) {
        counter--
        divCounterVideo.innerText = counter.toString()
      } else if (counter == 0) {
        divCounterVideo.style.visibility = 'hidden'
        clearInterval(interval)
        counter = 3
        divVideoBigParent.style.top = '0'
        divVideoBigContent.firstChild.style.minWidth = '0'
        divVideoBigContent.firstChild.style.minHeight = '100%'
        setTimeout(function () {
          divCloseParent.style.visibility = 'visible'
          divVideoBigContent.childNodes[2].play()
          counterVideoText()
          divControlsParent.style.display = 'block'
          parentBody.style.cssText += 'overflow-y: hidden !important'
        }, 800)
      }
    }, 700)
  }
  var countClickPlay = 2
  iconVideoPlay.addEventListener('click', function () {
    if (countClickPlay % 2 == 0) {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_PLAY + '")'
      divVideoBigContent.childNodes[2].pause()
      clearInterval(intervalCounter)
    } else {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_PAUSE + '")'
      divVideoBigContent.childNodes[2].play()
      counterVideoText(divVideoBigContent.childNodes[2].currentTime)
    }
    countClickPlay++
  })
  var countClickVol = 2
  volIcon.addEventListener('click', function () {
    if (countClickVol % 2 == 0) {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_VOL + '")'
      divVideoBigContent.childNodes[2].volume = 1.0
      divVideoBigContent.childNodes[2].muted = false
    } else {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_VOL_MUTED + '")'
      divVideoBigContent.childNodes[2].volume = 0.0
      divVideoBigContent.childNodes[2].muted = true
    }
    countClickVol++
  })

  imgClose.addEventListener('click', function (e) {
    closeVideoBig(e)
  })

  // STYLES
  object_assign(divControlsParent.style, {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: '0',
    height: '36px',
    left: '0',
    width: '100%',
    zIndex: '2',
    position: 'absolute',
    margin: '0',
    padding: '0',
    display: 'none'
  })
  object_assign(divControlsFirstChild.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })
  object_assign(divControlPlay.style, {
    bottom: '0',
    left: '10px',
    height: '36px',
    width: '36px',
    zIndex: 2,
    position: 'absolute',
    margin: '0',
    padding: '0'
  })
  object_assign(iconVideoPlay.style, {
    color: '#cccccc',
    textAlign: 'right',
    cursor: 'pointer',
    textDecoration: 'none',
    display: 'inline-block',
    fontFamily: 'SASFont',
    fontSize: ' 1.5em',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0',
    backgroundImage: 'url("' + ICON_BUTTON_PAUSE + '")',
    backgroundSize: '100% 100%'
  })
  object_assign(divControlVol.style, {
    bottom: '0',
    height: '34px',
    width: '36px',
    zIndex: 2,
    position: 'absolute',
    margin: '0',
    padding: '0',
    right: '10px'
  })
  object_assign(volIcon.style, {
    color: '#cccccc',
    textAlign: 'right',
    cursor: 'pointer',
    textDecoration: 'none',
    display: 'inline-block',
    fontFamily: 'SASFont',
    fontSize: ' 1.5em',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0',
    backgroundImage: 'url("' + ICON_BUTTON_VOL_MUTED + '")',
    backgroundSize: '100% 100%'
  })
  object_assign(countDownVideo.style, {
    height: '36px',
    bottom: '0',
    left: '0',
    width: '100%',
    zIndex: '1',
    position: 'absolute',
    margin: '0',
    padding: '0'
  })
  object_assign(countDownChild.style, {
    color: '#cccccc',
    textAlign: 'center',
    textDecoration: 'none',
    fontFamily: 'Gotham, Arial',
    fontSize: ' 14px',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0'
  })
  object_assign(divVideoSmallParent.style, {
    width: '300px',
    height: '250px',
    position: 'relative',
    top: '0px',
    left: '0px',
    right: '0',
    margin: '5px auto',
    zIndex: '999',
    cursor: 'pointer'
  })
  object_assign(divVideoSmall.style, {
    height: '100%',
    left: '0',
    position: 'absolute !important',
    top: '0',
    width: '100%',
    zIndex: '101',
    backgroundColor: '#000'
  })
  object_assign(divCounterVideo.style, {
    left: '130px',
    top: '102px',
    visibility: 'hidden',
    width: '40px',
    height: '46px',
    zIndex: '1000',
    position: 'absolute',
    background: '#000',
    opacity: '.8',
    color: '#ffffff',
    font: '36px Verdana,sans-serif',
    textAlign: 'center',
    pointerEvents: 'none'
  })
  object_assign(anchorClickVideoSmall.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1',
    backgroundColor: 'rgba(255,255,255,0)',
    cursor: 'pointer'
  })
  object_assign(divVideoBigParent.style, {
    position: 'fixed',
    width: '100%', //windowParentWidth + 'px'
    height: '100%', //windowParentHeigth + 'px'
    zIndex: '9999999',
    marginLeft: 'auto',
    left: '0',
    marginRight: 'auto',
    right: '0',
    top: '100%',//windowParentHeigth + 'px',
    marginTop: '0',
    webkitTransition: 'top 0.7s ease',
    transition: 'top 0.7s ease'
  })
  object_assign(divVideoBigFirstChild.style, {
    position: 'relative',
    width: '100%',//windowParentWidth + 'px'
    height: '100%', //windowParentHeigth + 'px'
    zIndex: '9999999'
  })
  object_assign(divVideoBigSecondChild.style, {
    position: 'absolute',
    width: '100%',//windowParentWidth + 'px'
    height: '100%', //windowParentHeigth + 'px'
    zIndex: '9999999'
  })
  object_assign(divVideoBigThirdChild.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '101',
    backgroundColor: '#000'
  })
  object_assign(divVideoBigFourthChild.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })
  object_assign(divVideoBigFithChild.style, {
    margin: '0',
    padding: '0',
    width: '100%',
    height: '100%',
    left: '0',
    top: '0',
    overflowX: 'hidden',
    overflowY: 'hidden',
    position: 'absolute',
    zIndex: '1'
  })
  object_assign(divVideoBigContent.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })

  object_assign(anchorClickVideoBig.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1',
    margin: '0',
    padding: '0',
    backgroundColor: 'rgba(255,255,255,0)',
    cursor: 'pointer'
  })
  object_assign(divCloseParent.style, {
    position: 'fixed',
    top: '30px',
    right: '120px',
    cursor: 'pointer',
    fontFamily: 'Arial,sans-serif',
    fontSize: '14px',
    zIndex: '10000000',
    color: '#000000',
    visibility: 'hidden'
  })
  object_assign(imgClose.style, {
    marginLeft: '10px',
    zIndex: '10000000',
    width: '50px',
    height: '50px',
    webkitTapHighlightColor: 'rgba(0,0,0,0)',
    webkitUserSelect: 'none',
    cursor: 'pointer'
  })
  divVideoBigParent.style.setProperty('-moz-transition', 'top 0.7s ease')
  divVideoBigParent.style.setProperty('-ms-transition', 'top 0.7s ease')
  divVideoBigParent.style.setProperty('-o-transition', 'top 0.7s ease')

  // appends
  setAnchorTo(divVideoSmall, anchorClickVideoSmall)
  divVideoSmallParent.appendChild(divVideoSmall)
  divVideoSmallParent.appendChild(divCounterVideo)
  document.body.appendChild(divVideoSmallParent)

  divVideoBigParent.appendChild(divVideoBigFirstChild) // top: 0
  divVideoBigFirstChild.appendChild(divVideoBigSecondChild)
  divVideoBigSecondChild.appendChild(divVideoBigThirdChild)
  divVideoBigThirdChild.appendChild(divVideoBigFourthChild)
  divVideoBigFourthChild.appendChild(divVideoBigFithChild)
  divVideoBigFithChild.appendChild(divVideoBigContent)
  setAnchorTo(divVideoBigContent, anchorClickVideoBig)
  imgClose.src = IMG_CLOSE
  divCloseParent.appendChild(imgClose)

  divControlPlay.appendChild(iconVideoPlay)
  divControlsFirstChild.appendChild(divControlPlay)
  divControlVol.appendChild(volIcon)
  countDownVideo.appendChild(countDownChild)
  divControlsFirstChild.appendChild(countDownVideo)
  divControlsFirstChild.appendChild(divControlVol)
  divControlsParent.appendChild(divControlsFirstChild)

  divVideoBigFourthChild.appendChild(divControlsParent)
  divVideoBigContent.appendChild(divCloseParent)
  parentBody.appendChild(divVideoBigParent)

  const makeMediaTag = function (divToappend, mediaToSet, stylesToset, isVideoBig = false, EXTENSION_TYPE) {
    switch (EXTENSION_TYPE) {
      case 'image/gif/png':
        var imgTag = document.createElement('img')
        imgTag.src = mediaToSet + '?' + CACHEBUSTER
        object_assign(imgTag.style, {
          height: isVideoBig ? (stylesToset[1] * 1.27).toString() + 'px' : stylesToset[1],
          width: stylesToset[0],
          marginLeft: stylesToset[2],
          marginTop: stylesToset[3],
          minHeight: stylesToset[5],
          minWidth: stylesToset[4]
        })
        divToappend.appendChild(imgTag)
        break
      case 'video':
        var videoTag = document.createElement('video')
        videoTag.src = mediaToSet + '?' + CACHEBUSTER
        videoTag.playsinline = 'playsinline'
        videoTag.preload = true
        videoTag.muted = true
        videoTag.autoplay = !isVideoBig
        videoTag.loop = !isVideoBig
        object_assign(videoTag.style, {
          height: stylesToset[1],
          marginLeft: stylesToset[2],
          marginTop: stylesToset[3],
          minHeight: stylesToset[5],
          minWidth: stylesToset[4]
        })

        if (isVideoBig) {
          videoTag.style.width = stylesToset[0]
          videoTag.style.objectFit = 'cover'
          videoTag.addEventListener('ended', function () {
            closeVideoBig()
          })
        } else {
          videoTag.style.cssText += 'width: 100% !important'
        }
        divToappend.appendChild(videoTag)
        break
      case 'HTML':
        var iframeTag = document.createElement('iframe')
        iframeTag.src = mediaToSet + '?' + CACHEBUSTER
        iframeTag.width = stylesToset[0]
        iframeTag.height = isVideoBig ? (stylesToset[1] * 1.27).toString() + 'px' : stylesToset[1]
        iframeTag.scrolling = 'no'
        iframeTag.frameborder = '0'
        object_assign(iframeTag.style, {
          marginLeft: stylesToset[2],
          marginTop: stylesToset[3],
          minHeight: stylesToset[5],
          minWidth: stylesToset[4]
        })
        divToappend.appendChild(iframeTag)
      default:
        console.log('The template' + CREATIVE_ID + 'doesnt suppot this format')
    }
  }
  divCounterVideo.innerText = counter.toString()
  divVideoSmall.addEventListener('mouseover', function () {
    decreaseCounter()
    this.childNodes[1].muted = false
    this.childNodes[1].volume = 1.0
  })

  divVideoSmall.addEventListener('mouseout', function () {
    resetCounter()
    this.childNodes[1].muted = true
    this.childNodes[1].volume = 0.0
  })

  makeMediaTag(divVideoSmall, MEDIA_VIDEO_IMG_SHORT, ['100%', '100%', '0', '0', '100%', '0'], false, EXTENSION_TYPE)
  makeMediaTag(divVideoBigContent, MEDIA_VIDEO_IMG_BIG, ['100%', '100%', '0', '0', '100%', '100%'], true, EXTENSION_TYPE)
  console.log(MEDIA_URL)
})()
