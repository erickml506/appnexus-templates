(function(){
    var
    CACHEBUSTER = '${CACHEBUSTER}',
    CREATIVE_ID = '${CREATIVE_ID}',
    MEDIA_URL = '${MEDIA_URL}',
    CREATIVE_WIDTH = '${CREATIVE_WIDTH}',
    CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}',
    CLICK_URL = '${CLICK_URL}',
    URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}',
    MEDIA_ZOCALO_LEFT_1 = '#{MEDIA_ZOCALO_LEFT_1}',
    MEDIA_ZOCALO_LEFT_2 = '#{MEDIA_ZOCALO_LEFT_2}',
    MEDIA_ZOCALO_RIGHT_1 = '#{MEDIA_ZOCALO_RIGHT_1}',
    MEDIA_ZOCALO_RIGHT_2 = '#{MEDIA_ZOCALO_RIGHT_2}',
    EXTENSION_TYPE_ZOCALOS = '#{EXTENSION_TYPE_ZOCALOS}',
    EXTENSION_TYPE_LAYER = '#{EXTENSION_TYPE_LAYER}',
    MEDIA_LAYER_1 = '#{MEDIA_LAYER_1}',
    MEDIA_LAYER_2 = '#{MEDIA_LAYER_2}';



    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    const getWidthOrHeight = function(size, wh){
        return  size.toLocaleLowerCase()
                    .split('x')[wh == 'width' ? 0 : 1] + 'px';
    }
    const object_assign = function() {
        var obj = arguments[0]
        for (var index = 1; index < arguments.length; index++) {
            var args = arguments[index]
            for (field in args) {
                obj[field] = args[field]
            }
        }
        return obj
    }

    const setAnchorTo = function(divToappend, anchor_click){
        divToappend.appendChild(anchor_click);
        anchor_click.addEventListener("click", function(){
          window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT),'_blank')
        })
    }

    const makeMediaTag = function(EXTENSION_TYPE, URL_SRC){      
        var mediaTag;
        switch (EXTENSION_TYPE) {
            case 'image/gif/png':
                mediaTag = document.createElement('img');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                break;
            case 'HTML':
                mediaTag = document.createElement('iframe');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                mediaTag.width = '100%';
                mediaTag.height = '100%';
                mediaTag.scrolling = 'no';
                mediaTag.frameborder = '0';
                mediaTag.style.border = '0'
                break;
            default:
                console.log("The template" + CREATIVE_ID + 'doesnt suppot this format')
        }
        return mediaTag;
    }

    var parentDocument = document.body.ownerDocument.defaultView.parent.document,
        parentBody = document.body.ownerDocument.defaultView.parent.document.body,
        div_parent = document.createElement('div'),
        div_parent_zocalos = document.createElement('div'),
        div_zocalo_right = document.createElement('div'),
        div_zocalo_left = document.createElement('div'),
        div_parent_layer = document.createElement('div'),
        tag_zocalo_left_1 = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_LEFT_1),
        tag_zocalo_left_2 = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_LEFT_2),
        tag_zocalo_right_1 = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_RIGHT_1),
        tag_zocalo_right_2 = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_RIGHT_2),
        tag_layer_1 = makeMediaTag(EXTENSION_TYPE_LAYER, MEDIA_LAYER_1),
        tag_layer_2 = makeMediaTag(EXTENSION_TYPE_LAYER, MEDIA_LAYER_2),
        layer_anchor_click = document.createElement('a'),
        anchor_click_zocalo_left = document.createElement('a'),
        anchor_click_zocalo_right = document.createElement('a');

    //styles
    parentBody.classList.add("tc");

    [tag_zocalo_left_1, tag_zocalo_left_2, tag_zocalo_right_1, tag_zocalo_right_2].forEach(function(tag, i){
        object_assign(tag.style,{
            position: 'absolute',
            width: '100%',
            height: '100%'
        });
        i==1 || i == 3 ? tag.style.display = 'none': void 0;
    })

    object_assign(anchor_click_zocalo_left.style, {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: '99',
        cursor: 'pointer'
    });
    object_assign(anchor_click_zocalo_right.style, {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: '99',
        cursor: 'pointer'
    })
    object_assign(div_parent.style, {
        position: 'fixed',
        width: '100%',
        height: '0px',
        zIndex: '999999999999',
        display: 'block'
    });
    object_assign(div_parent_zocalos.style, {
        position: 'relative',
        width: '1px',
        height: '1px',
        margin: '0 auto'
    });

    object_assign(div_zocalo_left.style, {
        position: 'absolute',
        top: '46px',
        left: '-662px',
        width: '150px',
        height: '600px',
        display: 'block',
        zIndex: '998',
    });

    
    object_assign(div_zocalo_right.style, {
        position: 'absolute',
        top: '46px',
        left: '512px',
        width: '150px',
        height: '600px',
        display: 'block',
        zIndex: '998',
    }); 


    object_assign(div_parent_layer.style, {
        width: CREATIVE_WIDTH+'px',
        height: CREATIVE_HEIGHT+'px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        margin: '0 auto',
        zIndex: '999',
        cursor: 'pointer',
        display: 'block',
    });
    
    object_assign(tag_layer_1.style, {
        width: '100%',
        height: '100%',
        position:'absolute',
        top: '0'
    }); 
    object_assign(tag_layer_2.style, {
        width: '100%',
        height: '100%',
        position:'absolute',
        top: '0'
    }); 

    object_assign(layer_anchor_click.style, {
        height: '100%',
        left: '0',
        position: 'absolute',
        top: '0',
        width: '100%',
        zIndex: '1',
        backgroundColor: 'rgba(255, 255, 255, 0)',
        cursor: 'pointer'
    }); 

    //events
    var tagToSetEvents = [div_zocalo_left, div_zocalo_right, div_parent_layer];
    tagToSetEvents.forEach(function(el, i){
        el.addEventListener('mouseover', function(){
            tag_zocalo_left_2.style.display = 'block';
            tag_zocalo_right_2.style.display = 'block';
            tag_layer_2.style.display = 'block';
            tag_zocalo_left_1.style.display = 'none';
            tag_zocalo_right_1.style.display = 'none';
            tag_layer_1.style.display = 'none';
           
        });
        el.addEventListener('mouseout', function(){
            tag_zocalo_left_1.style.display = 'block';
            tag_zocalo_right_1.style.display = 'block';
            tag_layer_1.style.display = 'block';

            tag_zocalo_left_2.style.display = 'none';
            tag_zocalo_right_2.style.display = 'none';
            tag_layer_2.style.display = 'none';
        });
    });

    
    div_parent_layer.appendChild(tag_layer_2);
    div_parent_layer.appendChild(tag_layer_1);
    setAnchorTo(div_parent_layer, layer_anchor_click);
    document.body.appendChild(div_parent_layer);

    div_zocalo_left.appendChild(tag_zocalo_left_2);
    div_zocalo_left.appendChild(tag_zocalo_left_1);
    setAnchorTo(div_zocalo_left, anchor_click_zocalo_left);
    div_parent_zocalos.appendChild(div_zocalo_left);

    div_zocalo_right.appendChild(tag_zocalo_right_2);
    div_zocalo_right.appendChild(tag_zocalo_right_1);
    setAnchorTo(div_zocalo_right, anchor_click_zocalo_right);

    div_parent_zocalos.appendChild(div_zocalo_right);
    div_parent.appendChild(div_parent_zocalos);
    parentBody.insertBefore(div_parent, parentBody.childNodes[0]);

})()