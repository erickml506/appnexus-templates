(function () {
  var CACHEBUSTER = '${CACHEBUSTER}'
  var CLICK_URL = '${CLICK_URL}'
  var MEDIA_URL = '${MEDIA_URL}'
  var CLICK_URL_ENC = '${CLICK_URL_ENC}'
  var CREATIVE_WIDTH = parseInt('${CREATIVE_WIDTH}')
  var CREATIVE_HEIGHT = parseInt('${CREATIVE_HEIGHT}')
  var BUTTON_CLOSE = '#{BUTTON_CLOSE}'
  var BUTTON_CLOSE_WIDTH = parseInt('#{BUTTON_CLOSE_WIDTH}')
  var BUTTON_CLOSE_HEIGHT = parseInt('#{BUTTON_CLOSE_HEIGHT}')
  var BUTTON_CLOSE_RIGHT = parseInt('#{BUTTON_CLOSE_RIGHT}')
  var BUTTON_CLOSE_TOP = parseInt('#{BUTTON_CLOSE_TOP}')
  var LAYER_WIDTH = parseInt('#{LAYER_WIDTH}')
  var LAYER_HEIGHT = parseInt('#{LAYER_HEIGHT}')
  var LAYER_COLOR = '#{LAYER_COLOR}'
  var LAYER_IMAGE = '#{LAYER_IMAGE}'
  var IS_LAYER_HTML = JSON.parse('#{IS_LAYER_HTML}')
  var LAYER_URL = '#{LAYER_URL}'
  var LANDING_PAGE_URL = '#{LANDING_PAGE_URL}'
  var IS_BANNER_HTML = JSON.parse('#{IS_BANNER_HTML}')
  var BANNER_URL = '#{BANNER_URL}'
  var USE_LANDING_URL = JSON.parse('#{USE_LANDING_URL}')
  var USE_LANDING_URL_CASE = '#{USE_LANDING_URL_CASE}'
  var form = '#{FORM_BUTTON_ID}'
  var utils = {
    parsePixel: function (value) {
      return (isNaN(value)) ? value : (value + 'px')
    },
    parsePercentage: function (value) {
      return (isNaN(value)) ? value : (value + '%')
    }
  }
  const makeUrlRedirect = function(url, urlredir){
    urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
    return urlredir;
  }
  /* BANNER */
  var divBanner = document.createElement('div');
  var anchorClickBanner = document.createElement('a')

  object_assign(anchorClickBanner.style, {
    cursor: 'pointer',
    height: utils.parsePixel(CREATIVE_HEIGHT),
    left: '0',
    position: 'absolute',
    top: '0',
    width: utils.parsePixel(CREATIVE_WIDTH),
    zIndex: '10'
  })

  const setAnchorToLayer = function(){
    divLayer.appendChild(anchorClickLayer);
    anchorClickLayer.addEventListener("click", function(){
      window.open(makeUrlRedirect(CLICK_URL,LANDING_PAGE_URL),'_blank')
    })
  }
  const requestToMakeTrack = function(){
    var s = document.createElement('script');
    s.src = 'http://ib.adnxs.com/ttj?id=1812103&cb='+CACHEBUSTER+'&pubclick='+CLICK_URL_ENC
    s.type='text/javascript'
    document.body.appendChild(s);
  }
  anchorClickBanner.addEventListener('click', function () {   
    USE_LANDING_URL ? setAnchorToLayer()  : null;
    requestToMakeTrack();
    document.body.ownerDocument.defaultView.parent.document.body.appendChild(divLayer)
  })

  if (IS_BANNER_HTML) {

    var iframeBanner = document.createElement('iframe')
    iframeBanner.scrolling = 'no'
    iframeBanner.src = BANNER_URL //+ '?' + CACHEBUSTER
    object_assign(iframeBanner, {
      height: utils.parsePixel(CREATIVE_HEIGHT),
      width: utils.parsePixel(CREATIVE_WIDTH)
    })
    object_assign(iframeBanner.style, {
      border: 0
    })

    divBanner.appendChild(iframeBanner)
  } else {
    var imgBanner = document.createElement('img')
    imgBanner.src = MEDIA_URL + '?' + CACHEBUSTER
    divBanner.appendChild(imgBanner)
  }

  divBanner.appendChild(anchorClickBanner)
  document.body.appendChild(divBanner)

  //LAYER
  var divLayer = document.createElement('div')
  divLayer.id = 'DivLayer'
  var divCloseLayer = document.createElement('div')
  var anchorClickLayer = document.createElement('a')

  object_assign(anchorClickLayer.style, {
    cursor: 'pointer',
    height: utils.parsePixel(CREATIVE_HEIGHT),
    left: '0',
    position: 'absolute',
    top: '0',
    width: utils.parsePixel(CREATIVE_WIDTH),
    zIndex: '10'
  })
  object_assign(divLayer.style, {
    height: utils.parsePixel(LAYER_HEIGHT),
    left: '50%',
    marginLeft: utils.parsePixel(-LAYER_WIDTH / 2),
    marginTop: utils.parsePixel(-LAYER_HEIGHT / 2),
    position: 'fixed',
    top: '50%',
    width: utils.parsePixel(LAYER_WIDTH),
    zIndex: '99999999'
  })

  object_assign(divCloseLayer.style, {
    cursor: 'pointer',
    height: utils.parsePixel(BUTTON_CLOSE_HEIGHT),
    position: 'absolute',
    right: utils.parsePixel(BUTTON_CLOSE_RIGHT),
    top: utils.parsePixel(BUTTON_CLOSE_TOP),
    width: utils.parsePixel(BUTTON_CLOSE_WIDTH),
    zIndex: '20',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'canter'
  })

  if (BUTTON_CLOSE !== '') {
    var imgCloseLayer = document.createElement('img')
    imgCloseLayer.src = BUTTON_CLOSE
    imgCloseLayer.style.width = "30px"
    divCloseLayer.appendChild(imgCloseLayer)
  } else {
    divCloseLayer.innerText = "X"
  }

  if (IS_LAYER_HTML) {
    var iframeLayer = document.createElement('iframe')
    iframeLayer.scrolling = 'no'
    iframeLayer.id = 'iframeLayer'
    iframeLayer.src = LAYER_URL + '?' + CACHEBUSTER
    object_assign(iframeLayer, {
      height: utils.parsePixel(LAYER_HEIGHT),
      width: utils.parsePixel(LAYER_WIDTH)
    })
    object_assign(iframeLayer.style, {
      border: 0
    })
    divLayer.appendChild(iframeLayer)
  } else {
    var imgLayer = document.createElement('img')
    imgLayer.src = LAYER_IMAGE + '?' + CACHEBUSTER
    imgLayer.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL,LANDING_PAGE_URL), '_blank')
    })
    divLayer.appendChild(imgLayer)
  }

  divCloseLayer.addEventListener('click', function () {
    var $divLayer = document.body.ownerDocument.defaultView.parent.document.getElementById('DivLayer')
    $divLayer.parentNode.removeChild($divLayer)
  })

  divLayer.appendChild(divCloseLayer)




  function object_assign() {
    var obj = arguments[0]
    for (var index = 1; index < arguments.length; index++) {
      var args = arguments[index]
      for (field in args) {
        obj[field] = args[field]
      }
    }
    return obj
  }
})()