(function(){
  
    var MEDIA_URL = '${MEDIA_URL}'
    var CREATIVE_WIDTH = parseInt('${CREATIVE_WIDTH}')
    var CREATIVE_HEIGHT = parseInt('${CREATIVE_HEIGHT}')
    var CACHEBUSTER = '${CACHEBUSTER}'
  
    var FRAME_TAKEOVER = '#{FRAME_TAKEOVER}'
    var FRAME_TAKEOVER_WIDTH = '#{FRAME_TAKEOVER_WIDTH}'
    var FRAME_TAKEOVER_HEIGHT = '#{FRAME_TAKEOVER_HEIGHT}'
  
    var BUTTON_COLLAPSE_WIDTH = parseInt('#{BUTTON_COLLAPSE_WIDTH}')
    var BUTTON_COLLAPSE_HEIGHT = parseInt('#{BUTTON_COLLAPSE_HEIGHT}')
    var BUTTON_COLLAPSE_RIGHT = parseInt('#{BUTTON_COLLAPSE_RIGHT}')
    var BUTTON_COLLAPSE_TOP = parseInt('#{BUTTON_COLLAPSE_TOP}')
  
    var BUTTON_EXPAND_WIDTH = parseInt('#{BUTTON_EXPAND_WIDTH}')
    var BUTTON_EXPAND_HEIGHT = parseInt('#{BUTTON_EXPAND_HEIGHT}')
    var BUTTON_EXPAND_RIGHT = parseInt('#{BUTTON_EXPAND_RIGHT}')
    var BUTTON_EXPAND_TOP = parseInt('#{BUTTON_EXPAND_TOP}')
    var CLICK_URL = '${CLICK_URL}'
    var CLICK_URL_REDIRECT = '#{CLICK_URL_REDIRECT}'
  
    var TIME = parseInt('#{TIME}')
  
    const
      rd = CACHEBUSTER,
      utils = {
        parse: function (value) {
          return (isNaN(value)) ? value : (value + "px")
        }
      };
    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    var idAnimationTimeout = null
  
    var executingAnimation = false
    var screenWidth = window.parent.innerWidth
    var screenHeight = window.parent.innerHeight
    var currentLeft = screenWidth
    var finalEnter = 0
    var finalExit = screenWidth
    var firstTime = true
  
    var parentBody = document.body.ownerDocument.defaultView.parent.document.body
  
    var divBanner = document.createElement('div')
    var divButtonExpand = document.createElement('div')
    var anchorBannerClick = document.createElement('a')
    var iframeBannerContent = document.createElement('iframe')
  
    var divLayer = document.createElement('div')
    divLayer.id = "divLayer"
    var divTakeOver = document.createElement('div')
    var divButtonCollapse = document.createElement('div')
    var anchorTakeOverClick = document.createElement('a')
    var iframeTakeOverContent = document.createElement('iframe')
    //LAST CHANGES
    object_assign(divBanner.style, {
      position: 'relative'
    })
  
    anchorBannerClick.href = makeUrlRedirect(CLICK_URL, CLICK_URL_REDIRECT)
    anchorBannerClick.target = '_blank'
  
    object_assign(anchorBannerClick.style, {
      curso: 'pointer',
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      zIndex: '1'
    });
    object_assign(iframeBannerContent.style, {
      border: '0px',
      height: utils.parse(CREATIVE_HEIGHT),
      width: utils.parse(CREATIVE_WIDTH)
    });
    object_assign(divBanner.style, {
      height: utils.parse(CREATIVE_HEIGHT),
      position: 'absolute',
      width: utils.parse(CREATIVE_WIDTH)
    });
    object_assign(iframeBannerContent, {
      frameborder: '0',
      height: CREATIVE_HEIGHT,
      marginheight: '0',
      marginwidth: '0',
      scrolling: 'no',
      tabindex: '-1',
      width: CREATIVE_WIDTH
    });
    document.body.appendChild(divBanner)
    divBanner.appendChild(divButtonExpand)
    divBanner.appendChild(anchorBannerClick)
    divBanner.appendChild(iframeBannerContent)
    iframeBannerContent.src = MEDIA_URL

    if ('#{BUTTON_EXPAND}' && '#{BUTTON_EXPAND}' != '\#\{BUTTON_EXPAND\}') {
      var imgButtonExpand = document.createElement('img')
      imgButtonExpand.src = '#{BUTTON_EXPAND}'
      object_assign(imgButtonExpand.style, {
        position: 'absolute',
        right: '0px',
        top: '0px'
      })
      divButtonExpand.appendChild(imgButtonExpand)
    }
  
  
    object_assign(divTakeOver.style, {
      position: 'relative'
    })
  
    anchorTakeOverClick.href = makeUrlRedirect(CLICK_URL, CLICK_URL_REDIRECT)
    anchorTakeOverClick.target = '_blank'
  
    object_assign(anchorTakeOverClick.style, {
      curso: 'pointer',
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      zIndex: '1'
    })
    iframeTakeOverContent.src = FRAME_TAKEOVER
    object_assign(iframeTakeOverContent.style, {
      border: '0px',
      height: utils.parse(FRAME_TAKEOVER_HEIGHT),
      width: utils.parse(FRAME_TAKEOVER_WIDTH)
    })
    object_assign(iframeTakeOverContent, {
      frameborder: '0',
      height: utils.parse(FRAME_TAKEOVER_HEIGHT),
      marginheight: '0',
      marginwidth: '0',
      scrolling: 'no',
      tabindex: '-1',
      width: utils.parse(FRAME_TAKEOVER_WIDTH)
    });
    var win = divLayer.ownerDocument.defaultView.parent
    object_assign(divTakeOver.style, {
      height: utils.parse(FRAME_TAKEOVER_HEIGHT),
      left: utils.parse((win.innerWidth / 2) - (FRAME_TAKEOVER_WIDTH / 2)),
      position: 'absolute',
      top: utils.parse((win.innerHeight / 2) - (FRAME_TAKEOVER_HEIGHT / 2)),
      width: utils.parse(FRAME_TAKEOVER_WIDTH)
    });
    object_assign(divLayer.style, {
      backgroundColor: 'white',
      height: '100%',
      left: utils.parse(finalExit),
      position: 'absolute',
      textAlign: 'left',
      top: '0px',
      verticalAlign: 'middle',
      visibility: 'visible',
      width: '100%',
      zIndex: '999999999999'
    });
    object_assign(divButtonCollapse.style, {
      cursor: 'pointer',
      height: utils.parse(BUTTON_COLLAPSE_HEIGHT),
      position: 'absolute',
      right: utils.parse(BUTTON_COLLAPSE_RIGHT),
      top: utils.parse(BUTTON_COLLAPSE_TOP),
      width: utils.parse(BUTTON_COLLAPSE_WIDTH),
      zIndex: '2'
    })
  
    object_assign(divButtonExpand.style, {
      cursor: 'pointer',
      height: utils.parse(BUTTON_EXPAND_HEIGHT),
      position: 'absolute',
      right: utils.parse(BUTTON_EXPAND_RIGHT),
      top: utils.parse(BUTTON_EXPAND_TOP),
      width: utils.parse(BUTTON_EXPAND_WIDTH),
      zIndex: '2'
    })
  
    parentBody.appendChild(divLayer)
    divLayer.appendChild(divTakeOver)
    divTakeOver.appendChild(divButtonCollapse)
    divTakeOver.appendChild(anchorTakeOverClick)
    divTakeOver.appendChild(iframeTakeOverContent)
    iframeTakeOverContent.onclick = function () {
      window.open(makeUrlRedirect(CLICK_URL, CLICK_URL_REDIRECT))
    }
 

    if ('#{BUTTON_COLLAPSE}' && '#{BUTTON_COLLAPSE}' != '\#\{BUTTON_COLLAPSE\}') {
      var imgButtonCollapse = document.createElement('img')
      imgButtonCollapse.src = '#{BUTTON_COLLAPSE}'
      object_assign(imgButtonCollapse.style, {
        position: 'absolute',
        right: '0px',
        top: '0px'
      })
      divButtonCollapse.appendChild(imgButtonCollapse)
    }
  
  

    function expand() {
      object_assign(parentBody.style, {
        overflow: 'hidden'
      })
      object_assign(divLayer.style, {
        display: 'block',
        visibility: 'visible'
      })
      setTimeout(function () {
        if (firstTime) {
          window.parent.scroll(currentLeft, 0)
          currentLeft = 0
          firstTime = false
        } else {
          enderTakeOver()
        }
      }, 20)
      idAnimationTimeout = window.setTimeout(collapse, TIME * 1000)
    }
    divButtonExpand.onclick = expand
  
    function collapse() {
      idAnimationTimeout && clearTimeout(idAnimationTimeout)
      object_assign(parentBody.style, {
        overflow: null,
      })
      setTimeout(function () {
        exitTakeOver(function () {
          object_assign(divLayer.style, {
            display: 'none',
            visibility: 'hidden'
          })
        })
      }, 20)
    }
    divButtonCollapse.onclick = collapse
  
    function enderTakeOver(cb) {
      var done = false
      var newLeft = currentLeft - 30
  
      if (newLeft <= finalEnter) {
        newLeft = finalEnter
        done = true
      }
      currentLeft = newLeft
      window.parent.scroll(finalExit - currentLeft, 0)
      if (!done) {
        setTimeout(function () {
          enderTakeOver(cb)
        }, 15)
      } else {
        cb && cb()
      }
    }
  
    function exitTakeOver(cb) {
      var done = false
      var newLeft = currentLeft + 30
  
      if (newLeft >= finalExit) {
        newLeft = finalExit
        done = true
      }
      currentLeft = newLeft
      window.parent.scroll(finalExit - currentLeft, 0)
      if (!done) {
        setTimeout(function () {
          exitTakeOver(cb)
        }, 15)
      } else {
        cb && cb()
      }
    }
  
    if (TIME) {
      expand()
    } else {
      collapse()
    }
  
    function object_assign() {
      var obj = arguments[0]
      for (var index = 1; index < arguments.length; index++) {
        var args = arguments[index]
        for (field in args) {
          obj[field] = args[field]
        }
      }
      return obj
    }
  
})()

