(function () {
  var CACHEBUSTER = '${CACHEBUSTER}'
  var CREATIVE_ID = '${CREATIVE_ID}'
  var CREATIVE_WIDTH = '${CREATIVE_WIDTH}'
  var CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}'
  var MEDIA_URL = '${MEDIA_URL}'
  var CLICK_URL = '${CLICK_URL}'
  var URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}'
  var MEDIA_SKIN = '#{MEDIA_SKIN}'
  var MEDIA_VIDEO_IMG_SHORT = '#{MEDIA_VIDEO_IMG_SHORT}'
  var MEDIA_VIDEO_IMG_BIG = '#{MEDIA_VIDEO_IMG_BIG}'
  var EXTENSION_TYPE = '#{EXTENSION_TYPE}'
  var IMG_CLOSE = '#{IMG_CLOSE}'
  var ICON_BUTTON_PLAY = '#{ICON_BUTTON_PLAY}'
  var ICON_BUTTON_PAUSE = '#{ICON_BUTTON_PAUSE}'
  var ICON_BUTTON_VOL = '#{ICON_BUTTON_VOL}'
  var ICON_BUTTON_VOL_MUTED = '#{ICON_BUTTON_VOL_MUTED}'

  const makeUrlRedirect = function (url, urlredir) {
    urlredir = url.split('clickenc=')[0] + 'clickenc=' + urlredir
    return urlredir
  }
  const object_assign = function () {
    var obj = arguments[0]
    var field
    for (var index = 1; index < arguments.length; index++) {
      var args = arguments[index]
      for (field in args) {
        obj[field] = args[field]
      }
    }
    return obj
  }
  const setAnchorTo = function (divToappend, anchor_click) {
    divToappend.appendChild(anchor_click)
    anchor_click.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT), '_blank')
    })
  }
  const parse = function (val) {
    return (isNaN(val)) ? val : (val + 'px')
  }

  const resetCounter = function () {
    clearInterval(interval)
    divCounterVideo.style.visibility = 'hidden'
    counter = 3
  }

  // make tags
  var parentDocument = document.body.ownerDocument.defaultView.parent.document

  var parentBody = document.body.ownerDocument.defaultView.parent.document.body

  var parentHead = document.body.ownerDocument.defaultView.parent.document.head

  var win = document.body.ownerDocument.defaultView.parent

  var windowParentWidth = win.outerWidth

  var windowParentHeigth = win.innerHeight

  var windowParentHeigthOuter = win.outerHeight

  var currentBody = document.body

  var divParent = parentDocument.createElement('div')

  var divZocalosAndTop = parentDocument.createElement('div')

  var divZocalo1 = parentDocument.createElement('div')

  var divZocalo2 = parentDocument.createElement('div')

  var divVideoSmallParent = parentDocument.createElement('div')

  var divVideoSmall = parentDocument.createElement('div')

  var divCounterVideo = parentDocument.createElement('div')

  var anchorClickZocalo1 = document.createElement('a')

  var anchorClickZocalo2 = document.createElement('a')

  var anchorClickTop = document.createElement('a')

  var anchorClickVideoSmall = document.createElement('a')

  var counter = 3

  var divVideoBigParent = parentDocument.createElement('div')

  var divVideoBigFirstChild = parentDocument.createElement('div')

  var divVideoBigSecondChild = parentDocument.createElement('div')

  var divVideoBigThirdChild = parentDocument.createElement('div')

  var divVideoBigFourthChild = parentDocument.createElement('div')

  var divVideoBigFithChild = parentDocument.createElement('div')

  var divVideoBigContent = parentDocument.createElement('div')

  var anchorClickVideoBig = document.createElement('a')

  var divCloseParent = document.createElement('div')

  var imgClose = document.createElement('img')

  var divControlsParent = document.createElement('div')

  var divControlsFirstChild = document.createElement('div')

  var divControlPlay = document.createElement('div')

  var iconVideoPlay = document.createElement('a')

  var divControlVol = document.createElement('div')

  var volIcon = document.createElement('a')

  var countDownVideo = document.createElement('div')

  var countDownChild = document.createElement('div')

  parentBody.style.cssText += 'overflow-x: hidden !important'
  iconVideoPlay.id = 'iconVideoPlay'
  var interval,
    intervalCounter
  const closeVideoBig = function (e) {
    divVideoBigParent.style.top = windowParentHeigth + 'px'
    imgClose.parentElement.style.visibility = 'hidden'
    divVideoBigContent.childNodes[2].currentTime = 0
    divVideoBigContent.childNodes[2].pause()
    parentBody.style.overflowY = ''
    clearInterval(intervalCounter)
  }
  const counterVideoText = function () {
    intervalCounter = setInterval(function () {
      var countAd = divVideoBigContent.childNodes[2].duration - divVideoBigContent.childNodes[2].currentTime
      countDownChild.innerText = 'El anuncio terminará en ' + Math.round(countAd) + 's'
    }, 1000)
  }
  const decreaseCounter = function () {
    divCounterVideo.innerText = '3'
    divCounterVideo.style.visibility = 'visible'
    interval = setInterval(function () {
      if (counter > 0) {
        counter--
        divCounterVideo.innerText = counter.toString()
      } else if (counter === 0) {
        divCounterVideo.style.visibility = 'hidden'
        clearInterval(interval)
        counter = 3
        divVideoBigParent.style.top = '0'
        divVideoBigContent.firstChild.style.minWidth = '0'
        divVideoBigContent.firstChild.style.minHeight = '100%'
        setTimeout(function () {
          divCloseParent.style.visibility = 'visible'
          divVideoBigContent.childNodes[2].play()
          counterVideoText()
          divControlsParent.style.display = 'block'
          parentBody.style.overflowY = 'hidden'
        }, 800)
      }
    }, 700)
  }
  var countClickPlay = 2
  iconVideoPlay.addEventListener('click', function () {
    if (countClickPlay % 2 === 0) {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_PLAY + '")'
      divVideoBigContent.childNodes[2].pause()
      clearInterval(intervalCounter)
    } else {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_PAUSE + '")'
      divVideoBigContent.childNodes[2].play()
      counterVideoText(divVideoBigContent.childNodes[2].currentTime)
    }
    countClickPlay++
  })
  var countClickVol = 2
  volIcon.addEventListener('click', function () {
    if (countClickVol % 2 === 0) {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_VOL + '")'
      divVideoBigContent.childNodes[2].volume = 1.0
      divVideoBigContent.childNodes[2].muted = false
    } else {
      this.style.backgroundImage = 'url("' + ICON_BUTTON_VOL_MUTED + '")'
      divVideoBigContent.childNodes[2].volume = 0.0
      divVideoBigContent.childNodes[2].muted = true
    }
    countClickVol++
  })

  imgClose.addEventListener('click', function (e) {
    closeVideoBig(e)
  })
  object_assign(divControlsParent.style, {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: '0',
    height: '36px',
    left: '0',
    width: '100%',
    zIndex: '2',
    position: 'absolute',
    margin: '0',
    padding: '0',
    display: 'none'
  })
  object_assign(divControlsFirstChild.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })
  object_assign(divControlPlay.style, {
    bottom: '0',
    left: '0',
    height: '36px',
    width: '36px',
    zIndex: 2,
    position: 'absolute',
    margin: '0',
    padding: '0'
  })
  object_assign(iconVideoPlay.style, {
    color: '#cccccc',
    textAlign: 'right',
    cursor: 'pointer',
    textDecoration: 'none',
    display: 'inline-block',
    fontFamily: 'SASFont',
    fontSize: ' 1.5em',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0',
    backgroundImage: 'url("' + ICON_BUTTON_PAUSE + '")',
    backgroundSize: '100% 100%'
  })
  object_assign(divControlVol.style, {
    bottom: '0',
    height: '36px',
    width: '36px',
    zIndex: 2,
    position: 'absolute',
    margin: '0',
    padding: '0',
    right: '0'
  })
  object_assign(volIcon.style, {
    color: '#cccccc',
    textAlign: 'right',
    cursor: 'pointer',
    textDecoration: 'none',
    display: 'inline-block',
    fontFamily: 'SASFont',
    fontSize: ' 1.5em',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0',
    backgroundImage: 'url("' + ICON_BUTTON_VOL_MUTED + '")',
    backgroundSize: '100% 100%'
  })
  object_assign(countDownVideo.style, {
    height: '36px',
    bottom: '0',
    left: '0',
    width: '100%',
    zIndex: '1',
    position: 'absolute',
    margin: '0',
    padding: '0'
  })
  object_assign(countDownChild.style, {
    color: '#cccccc',
    textAlign: 'center',
    textDecoration: 'none',
    fontFamily: 'Gotham, Arial',
    fontSize: ' 14px',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '36px',
    lineHeight: '36px',
    verticalAlign: 'middle',
    width: '100%',
    webkitFontSmoothing: 'antialiased',
    margin: '0',
    padding: '0',
    outline: '0'
  })
  object_assign(divParent.style, {
    position: 'relative',
    width: '100%',
    height: '260px',
    zIndex: '10',
    display: 'block'
  })
  object_assign(divZocalosAndTop.style, {
    position: 'relative',
    width: '1px',
    height: '1px',
    margin: '0 auto'
  })
  object_assign(divZocalo1.style, {
    position: 'absolute',
    top: '0',
    left: '-662px',
    width: '150px',
    height: windowParentHeigth + 'px',
    display: 'block',
    zIndex: '998',
    border: 'none'
  })
  object_assign(divZocalo2.style, {
    position: 'absolute',
    top: '0',
    left: '512px',
    width: '150px',
    height: windowParentHeigth + 'px',
    display: 'block',
    zIndex: '998',
    border: 'none'
  })
  object_assign(divVideoSmallParent.style, {
    width: '300px',
    height: '235px',
    position: 'relative',
    top: '0px',
    left: '0px',
    right: '0',
    margin: '5px auto',
    zIndex: '999',
    cursor: 'pointer'
  })
  object_assign(divVideoSmall.style, {
    height: '100%',
    left: '0',
    position: 'absolute !important',
    top: '0',
    width: '100%',
    zIndex: '101',
    backgroundColor: '#000'
  })
  object_assign(divCounterVideo.style, {
    left: '130px',
    top: '102px',
    visibility: 'hidden',
    width: '40px',
    height: '46px',
    zIndex: '1000',
    position: 'absolute',
    background: '#000',
    opacity: '.8',
    color: '#ffffff',
    font: '36px Verdana,sans-serif',
    textAlign: 'center',
    pointerEvents: 'none'
  })
  object_assign(anchorClickZocalo1.style, {
    width: '150px',
    height: windowParentHeigth + 'px',
    display: 'block',
    zIndex: '998',
    border: 'none',
    cursor: 'pointer',
    position: 'fixed'
  })
  object_assign(anchorClickZocalo2.style, {
    width: '150px',
    height: windowParentHeigth + 'px',
    display: 'block',
    zIndex: '998',
    border: 'none',
    cursor: 'pointer',
    position: 'fixed'
  })
  object_assign(anchorClickTop.style, {
    position: 'absolute',
    top: '0',
    width: '1024px',
    height: '260px',
    left: '-512px',
    zIndex: '998',
    display: 'block',
    border: 'none',
    cursor: 'pointer'
  })
  object_assign(anchorClickVideoSmall.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1',
    backgroundColor: 'rgba(255,255,255,0)',
    cursor: 'pointer'
  })
  object_assign(divVideoBigParent.style, {
    position: 'fixed',
    width: '100%',//windowParentWidth + 'px',
    height: '100%',//windowParentHeigth + 'px',
    zIndex: '9999999',
    marginLeft: 'auto',
    left: '0',
    marginRight: 'auto',
    right: '0',
    top: windowParentHeigth + 'px',
    marginTop: '0',
    webkitTransition: 'top 0.7s ease',
    transition: 'top 0.7s ease'
  })
  divVideoBigParent.style.setProperty('-moz-transition', 'top 0.7s ease')
  divVideoBigParent.style.setProperty('-ms-transition', 'top 0.7s ease')
  divVideoBigParent.style.setProperty('-o-transition', 'top 0.7s ease')

  object_assign(divVideoBigFirstChild.style, {
    position: 'relative',
    width: '100%',//windowParentWidth + 'px',
    height: '100%',//windowParentHeigth + 'px',
    zIndex: '9999999'
  })
  object_assign(divVideoBigSecondChild.style, {
    position: 'absolute',
    width: '100%',//windowParentWidth + 'px',
    height: '100%',//windowParentHeigth + 'px',
    zIndex: '9999999'
  })
  object_assign(divVideoBigThirdChild.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '101',
    backgroundColor: '#000'
  })
  object_assign(divVideoBigFourthChild.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })
  object_assign(divVideoBigFithChild.style, {
    margin: '0',
    padding: '0',
    width: '100%',
    height: '100%',
    left: '0',
    top: '0',
    overflowX: 'hidden',
    overflowY: 'hidden',
    position: 'absolute',
    zIndex: '1'
  })
  object_assign(divVideoBigContent.style, {
    height: '100%',
    position: 'relative',
    width: '100%',
    margin: '0',
    padding: '0'
  })

  object_assign(anchorClickVideoBig.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1',
    margin: '0',
    padding: '0',
    backgroundColor: 'rgba(255,255,255,0)',
    cursor: 'pointer'
  })
  object_assign(divCloseParent.style, {
    position: 'fixed',
    top: '30px',
    right: '120px',
    cursor: 'pointer',
    fontFamily: 'Arial,sans-serif',
    fontSize: '14px',
    zIndex: '10000000',
    color: '#000000',
    visibility: 'hidden'
  })
  object_assign(imgClose.style, {
    marginLeft: '10px',
    zIndex: '10000000',
    width: '50px',
    height: '50px',
    webkitTapHighlightColor: 'rgba(0,0,0,0)',
    webkitUserSelect: 'none',
    cursor: 'pointer'
  })

  divZocalosAndTop.appendChild(divZocalo1)
  setAnchorTo(divZocalo1, anchorClickZocalo1)
  divZocalosAndTop.appendChild(divZocalo2)
  setAnchorTo(divZocalo2, anchorClickZocalo2)
  setAnchorTo(divZocalosAndTop, anchorClickTop)

  setAnchorTo(divVideoSmall, anchorClickVideoSmall)
  divVideoSmallParent.appendChild(divVideoSmall)
  divVideoSmallParent.appendChild(divCounterVideo)

  divParent.appendChild(divZocalosAndTop)
  divParent.appendChild(divVideoSmallParent)

  divVideoBigParent.appendChild(divVideoBigFirstChild) // top: 0
  divVideoBigFirstChild.appendChild(divVideoBigSecondChild)
  divVideoBigSecondChild.appendChild(divVideoBigThirdChild)
  divVideoBigThirdChild.appendChild(divVideoBigFourthChild)
  divVideoBigFourthChild.appendChild(divVideoBigFithChild)
  divVideoBigFithChild.appendChild(divVideoBigContent)
  setAnchorTo(divVideoBigContent, anchorClickVideoBig)
  imgClose.src = IMG_CLOSE
  divCloseParent.appendChild(imgClose)

  divControlPlay.appendChild(iconVideoPlay)
  divControlsFirstChild.appendChild(divControlPlay)
  divControlVol.appendChild(volIcon)
  countDownVideo.appendChild(countDownChild)
  divControlsFirstChild.appendChild(countDownVideo)
  divControlsFirstChild.appendChild(divControlVol)
  divControlsParent.appendChild(divControlsFirstChild)

  divVideoBigFourthChild.appendChild(divControlsParent)
  divVideoBigContent.appendChild(divCloseParent)
  parentBody.insertBefore(divParent, parentBody.childNodes[0])
  parentBody.appendChild(divVideoBigParent)

  object_assign(parentBody.style, {
    backgroundImage: 'url(' + MEDIA_SKIN + '?' + CACHEBUSTER + ')',
    backgroundPositionX: 'center',
    backgroundPositionY: 'top',
    backgroundSize: 'initial',
    backgroundRepeatX: 'no-repeat',
    backgroundRepeatY: 'no-repeat',
    backgroundAttachment: 'fixed',
    backgroundOrigin: 'initial',
    backgroundClip: 'initial',
    backgroundColor: 'rgb(255, 255, 255)',
    backgroundRepeat: 'no-repeat'
  })
  const makeMediaTag = function (divToappend, mediaToSet, stylesToset, isVideoBig = false) {
    switch (EXTENSION_TYPE) {
      case 'image/gif/png':
        var imgTag = document.createElement('img')
        imgTag.src = mediaToSet + '?' + CACHEBUSTER
        object_assign(imgTag.style, {
          height: isVideoBig ? (stylesToset[1] * 1.27).toString() + 'px' : stylesToset[1],
          width: stylesToset[0],
          marginLeft: stylesToset[2],
          marginTop: stylesToset[3],
          minHeight: stylesToset[5],
          minWidth: stylesToset[4]
        })
        divToappend.appendChild(imgTag)
        break
      case 'video':
        var videoTag = document.createElement('video')
        videoTag.src = mediaToSet + '?' + CACHEBUSTER
        videoTag.playsinline = 'playsinline'
        videoTag.preload = true
        videoTag.muted = true
        videoTag.autoplay = !isVideoBig
        videoTag.loop = !isVideoBig

        if (isVideoBig) {
          object_assign(videoTag.style, {
            width: '80%',
            height: '80%',
            objectFit: 'cover',
            top: '0',
            bottom: '0',
            left: '0',
            right: '0',
            margin: 'auto',
            position: 'absolute'
          })
          videoTag.addEventListener('ended', function () {
            closeVideoBig()
          })
        } else {
          object_assign(videoTag.style, {
            // isVideoBig ? (stylesToset[1] * 1.27).toString() + 'px' : stylesToset[1],
            height: stylesToset[1],
            marginLeft: stylesToset[2],
            marginTop: stylesToset[3],
            minHeight: stylesToset[5],
            minWidth: stylesToset[4]
          })
          videoTag.style.cssText += 'width: 333px !important'
        }
        divToappend.appendChild(videoTag)
        break
      case 'HTML':
        var iframeTag = document.createElement('iframe')
        iframeTag.src = mediaToSet + '?' + CACHEBUSTER
        iframeTag.width = stylesToset[0]
        iframeTag.height = isVideoBig ? (stylesToset[1] * 1.27).toString() + 'px' : stylesToset[1]
        iframeTag.scrolling = 'no'
        iframeTag.frameborder = '0'
        object_assign(iframeTag.style, {
          marginLeft: stylesToset[2],
          marginTop: stylesToset[3],
          minHeight: stylesToset[5],
          minWidth: stylesToset[4]
        })
        divToappend.appendChild(iframeTag)
      default:
        console.log('The template' + CREATIVE_ID + 'doesnt suppot this format')
    }
  }
  divCounterVideo.innerText = counter.toString()
  divVideoSmall.addEventListener('mouseover', function () {
    decreaseCounter()
    this.childNodes[1].muted = false
    this.childNodes[1].volume = 1.0
  })

  divVideoSmall.addEventListener('mouseout', function () {
    resetCounter()
    this.childNodes[1].muted = true
    this.childNodes[1].volume = 0.0
  })

  makeMediaTag(divVideoSmall, MEDIA_VIDEO_IMG_SHORT, ['333px !important', '235px', '-16px', '0', '100%', '0'])
  // [windowParentWidth + 'px', windowParentHeigthOuter , '0', '-248px', '100%', '100%']
  makeMediaTag(divVideoBigContent, MEDIA_VIDEO_IMG_BIG, [], true)// minW : 0 minH:100%
})()
