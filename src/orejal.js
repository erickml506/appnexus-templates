(function() {

    var
    CLICK_URL = '${CLICK_URL}',
    MEDIA_URL = '${MEDIA_URL}',
    CREATIVE_WIDTH = '${CREATIVE_WIDTH}',
    CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}',
    MEDIA_URL_COLAPSED_WIDTH = '#{MEDIA_URL_COLAPSED_WIDTH}',
    MEDIA_URL_COLAPSED_HEIGHT = '#{MEDIA_URL_COLAPSED_HEIGHT}',
    MEDIA_URL_EXPANDED = '#{MEDIA_URL_EXPANDED}',
    MEDIA_URL_EXPANDED_WIDTH = '#{MEDIA_URL_EXPANDED_WIDTH}',
    MEDIA_URL_EXPANDED_HEIGHT = '#{MEDIA_URL_EXPANDED_HEIGHT}',
    BUTTON_EXPAND = '#{BUTTON_EXPAND}' != '\#\{BUTTON_EXPAND\}'?'#{BUTTON_EXPAND}':'http://#',
    BUTTON_EXPAND_WIDTH = Number('#{BUTTON_EXPAND_WIDTH}'),
    BUTTON_EXPAND_HEIGHT = Number('#{BUTTON_EXPAND_HEIGHT}'),
    BUTTON_EXPAND_RIGHT = Number('#{BUTTON_EXPAND_RIGHT}'),
    BUTTON_EXPAND_TOP = Number('#{BUTTON_EXPAND_TOP}'),
    BUTTON_COLAPSE = '#{BUTTON_COLAPSE}' != '\#\{BUTTON_COLAPSE\}'?'#{BUTTON_COLAPSE}':'http://#',
    BUTTON_COLAPSE_WIDTH = Number('#{BUTTON_COLAPSE_WIDTH}'),
    BUTTON_COLAPSE_HEIGHT = Number('#{BUTTON_COLAPSE_HEIGHT}'),
    TIME = Number('#{TIME}'),
    IN_LAYER = '#{IN_LAYER}' == 'true'?true:false,
    LAYER_COLOR = '#{LAYER_COLOR}',
    POSITION_AS = '#{POSITION_AS}',
    IS_INTERATIVE = '#{IS_INTERATIVE}' == 'true'?true:false,
    URL_REDIRECT = '#{URL_REDIRECT}',
    FORMAT = '#{FORMAT}';
  

    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    var
    utils = {
      parse: function(value){
        return (isNaN(value))?value:(value + "px");
      }
    },
    parentBody = document.body.ownerDocument.defaultView.parent.document.body,
    currentBody = document.body,
    win = document.body.ownerDocument.defaultView.parent,
    divFloat = document.createElement('div'),
    container = divFloat,
    divLayer = document.createElement('div'),
  
    divColapsed = document.createElement('div'),
    divButtonExpand = document.createElement('div'),
    iframeColapsedContent = document.createElement('iframe'),
  
    divExpanded = document.createElement('div'),
    divButtonColapse = document.createElement('div'),
    iframeExpandedContent = document.createElement('iframe'),
  
    anchorClick = null;
  
    object_assign(divFloat.style, {
      position: 'fixed',
      zIndex: '9999999991'
    });
  
    container.appendChild(divColapsed);
    anchorClick = document.createElement('a');
    anchorClick.addEventListener('click', function() {
      window.open(makeUrlRedirect(CLICK_URL,URL_REDIRECT), '_blank');
    });
    object_assign(anchorClick.style, {
      position: 'absolute',
      top: '0px',
      left: '0px',
      width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
      height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT),
    });
    divColapsed.appendChild(anchorClick);
    //divColapsed.appendChild(iframeColapsedContent);
    iframeColapsedContent.src = MEDIA_URL;
    object_assign(iframeColapsedContent.style, {
      width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
      height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT),
      border: '0px'
    });
    object_assign(iframeColapsedContent, {
      width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
      height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT),
      tabindex: '-1',
      marginwidth: '0',
      marginheight: '0',
      scrolling: 'no',
      frameborder: '0'
    });
    object_assign(divColapsed.style, {
      position: 'absolute',
      width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
      height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT)
    });
    if (BUTTON_EXPAND != 'http://#') {
      if(FORMAT == 'img'){
        var imgButtonExpand = document.createElement('img');
        imgButtonExpand.src = BUTTON_EXPAND;
        object_assign(imgButtonExpand.style, {
          position: 'absolute',
          right: '0px',
          top: '0px'
        });
      }else{
        var imgButtonExpand = document.createElement('iframe');
        imgButtonExpand.src = BUTTON_EXPAND;
        object_assign(imgButtonExpand.style, {
          position: 'absolute',
          right: '0px',
          top: '0px',
          border:'0'
        });
        object_assign(imgButtonExpand, {
          width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
          height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT),
          tabindex: '-1',
          marginwidth: '0',
          marginheight: '0',
          scrolling: 'no',
          frameborder: '0'
        });
      }
      divButtonExpand.appendChild(imgButtonExpand);
    }
    divColapsed.appendChild(divButtonExpand);
  
    container.appendChild(divExpanded);
    anchorClick = document.createElement('a');
    anchorClick.addEventListener('click', function() {
      window.open(makeUrlRedirect(CLICK_URL,URL_REDIRECT), '_blank');
    });
    object_assign(anchorClick.style, {
      position: 'absolute',
      top: '0px',
      left: '0px',
      width: utils.parse(MEDIA_URL_EXPANDED_WIDTH),
      height: utils.parse(MEDIA_URL_EXPANDED_HEIGHT),
    });
    divExpanded.appendChild(anchorClick);
    divExpanded.appendChild(iframeExpandedContent);
    iframeExpandedContent.addEventListener('click', function() {
      window.open(makeUrlRedirect(CLICK_URL,URL_REDIRECT), '_blank');
    });
    iframeExpandedContent.src = MEDIA_URL_EXPANDED;
    object_assign(iframeExpandedContent.style, {
      width: utils.parse(MEDIA_URL_EXPANDED_WIDTH),
      height: utils.parse(MEDIA_URL_EXPANDED_HEIGHT),
      border: '0px'
    });
    object_assign(iframeExpandedContent, {
      width: utils.parse(MEDIA_URL_EXPANDED_WIDTH),
      height: utils.parse(MEDIA_URL_EXPANDED_HEIGHT),
      tabindex: '-1',
      marginwidth: '0',
      marginheight: '0',
      scrolling: 'no',
      frameborder: '0'
    });
    object_assign(divExpanded.style, {
      position: 'absolute',
      width: utils.parse(MEDIA_URL_EXPANDED_WIDTH),
      height: utils.parse(MEDIA_URL_EXPANDED_HEIGHT)
    });
    if (BUTTON_COLAPSE != 'http://#') {
      var imgButtonColapse = document.createElement('img');
      imgButtonColapse.src = BUTTON_COLAPSE;
      object_assign(imgButtonColapse.style, {
        position: 'absolute',
        right: '0px',
        top: '0px',
        width: '100%',
        height: '100%'
      });
      divButtonColapse.appendChild(imgButtonColapse);
    }
    divExpanded.appendChild(divButtonColapse);
  
    object_assign(divButtonColapse.style, {
      width: utils.parse(BUTTON_COLAPSE_WIDTH),
      height: utils.parse(BUTTON_COLAPSE_HEIGHT),
      left: '20px',
      top: '10px',
      position: 'absolute',
      cursor: 'pointer'
    });
  
    object_assign(divButtonExpand.style, {
      width: utils.parse(BUTTON_EXPAND_WIDTH),
      height: utils.parse(BUTTON_EXPAND_HEIGHT),
      right: utils.parse(BUTTON_EXPAND_RIGHT),
      top: utils.parse(BUTTON_EXPAND_TOP),
      position: 'absolute',
      cursor: 'pointer',
      zIndex: 999
    });
  
    function expand() {
      object_assign(divExpanded.style, {
        float: 'left',
        display: 'block'
      });
      object_assign(divColapsed.style, {
        display: 'none'
      });
      object_assign(divFloat.style, {
        width: utils.parse(MEDIA_URL_EXPANDED_WIDTH),
        height: utils.parse(MEDIA_URL_EXPANDED_HEIGHT)
      });
      if (IN_LAYER) {
        object_assign(divLayer.style, {
          display: 'block'
        });
        divLayer.appendChild(divFloat);
      }
      handlePosition();
    };
    divButtonExpand.onclick = expand;
  
    function colapse() {
      object_assign(divExpanded.style, {
        float: 'left',
        display: 'none'
      });
      object_assign(divColapsed.style, {
        display: 'block'
      });
      object_assign(divFloat.style, {
        width: utils.parse(MEDIA_URL_COLAPSED_WIDTH),
        height: utils.parse(MEDIA_URL_COLAPSED_HEIGHT)
      });
      if (IN_LAYER) {
        object_assign(divLayer.style, {
          display: 'none'
        });
        parentBody.appendChild(divFloat);
      }
      handlePosition();
    };
    divButtonColapse.onclick = colapse;
    colapse();
  
    if (IN_LAYER) {
      container = divLayer;
      object_assign(divLayer.style, {
        display: 'block',
        position: 'fixed',
        zIndex: 999,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        overflow: 'auto',
        backgroundColor: LAYER_COLOR
      });
      divLayer.appendChild(divFloat);
      parentBody.appendChild(divLayer);
    }
    else {
      parentBody.appendChild(divFloat);
    }
  
    if (TIME) {
      expand();
      window.setTimeout(colapse, TIME * 1000);
    }
  
    function handlePosition() {
      var
      centerH = utils.parse((win.innerWidth / 2) - (divFloat.clientWidth / 2)),
      centerV = utils.parse((win.innerHeight / 2) - (divFloat.clientHeight / 2));
     
      if (POSITION_AS == 'top_left') {
        object_assign(divFloat.style, {top: utils.parse(0), left: utils.parse(0)});
      }
      else if (POSITION_AS == 'top_center') {
        object_assign(divFloat.style, {left: centerH});
      }
      else if (POSITION_AS == 'top_right') {
        object_assign(divFloat.style, {top: utils.parse(0), right: utils.parse(0)});
      }
      else if (POSITION_AS == 'right_center') {
        object_assign(divFloat.style, {right: utils.parse(0), top: centerV});
      }
      else if (POSITION_AS == 'bottom_right') {
        object_assign(divFloat.style, {bottom: utils.parse(0), right: utils.parse(0)});
      }
      else if (POSITION_AS == 'bottom_center') {
        object_assign(divFloat.style, {bottom: utils.parse(0), left: centerH});
      }
      else if (POSITION_AS == 'bottom_left') {
        object_assign(divFloat.style, {bottom: utils.parse(0), left: utils.parse(0)});
      }
      else if (POSITION_AS == 'left_center') {
        object_assign(divFloat.style, {left: utils.parse(0), top: centerV});
      }
    }
  
    win.addEventListener("resize", handlePosition);
    handlePosition();
  
    var eventNames = [
      'mouseenter',
      'mouseout',
      'mouseover',
      'mouseleave',
      'click'
    ];
    /*var iframes = [iframeExpandedContent, iframeColapsedContent];
    for (var indexElement = 0; indexElement < iframes.length; indexElement++) {
      var element = iframes[indexElement].parentElement;
      for (var indexEvent = 0; indexEvent < eventNames.length; indexEvent++) {
        var eventName = eventNames[indexEvent];
        element.addEventListener(eventName, function(event) {
  
          for (var indexFrameElement = 0; indexFrameElement < iframes.length; indexFrameElement++) {
            var
            iframe = iframes[indexFrameElement],
            data = {
              action: 'adnx.template.event.'+event.type
            };
            iframe && iframe.contentWindow.postMessage(data, '*');
          }
  
        });
      }
    }*/
  
    if (IS_INTERATIVE) {
      divColapsed.addEventListener('mouseenter', function() {
        expand();
      });
      divExpanded.addEventListener('mouseout', function() {
        colapse();
      });
    }
  
    function object_assign() {
      var obj = arguments[0];
      for(var index = 1; index < arguments.length; index++) {
        var args = arguments[index];
        for (field in args) {
          obj[field] = args[field];
        }
      }
      return obj;
    };
  
  })();
  