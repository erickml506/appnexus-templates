(function () {
    var CACHEBUSTER = '${CACHEBUSTER}'
    var CLICK_URL = '${CLICK_URL}'
    var MEDIA_URL = '${MEDIA_URL}'
    var CREATIVE_WIDTH = parseInt('${CREATIVE_WIDTH}')
    var CREATIVE_HEIGHT = parseInt('${CREATIVE_HEIGHT}')
    var URL_REDIRECT_LANDING = '#{URL_REDIRECT_LANDING}'
    var BUTTON_CLOSE_ICON = '#{BUTTON_CLOSE_ICON}'
    var BUTTON_CLOSE_WIDTH = parseInt('#{BUTTON_CLOSE_WIDTH}')
    var BUTTON_CLOSE_HEIGHT = parseInt('#{BUTTON_CLOSE_HEIGHT}')
    var LAYER_WIDTH = parseInt('#{LAYER_WIDTH}')
    var LAYER_HEIGHT = parseInt('#{LAYER_HEIGHT}')
    var IS_LAYER_HTML = JSON.parse('#{IS_LAYER_HTML}')
    var LAYER_URL = '#{LAYER_URL}'
  
    var IS_BANNER_HTML = JSON.parse('#{IS_BANNER_HTML}')
    var BANNER_URL = '#{BANNER_URL}'
  
    var utils = {
      parsePixel: function (value) {
        return (isNaN(value)) ? value : (value + 'px')
      },
      parsePercentage: function (value) {
        return (isNaN(value)) ? value : (value + '%')
      }
    }
    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    
    var divLayer = document.createElement('div')
    divLayer.id = 'DivLayer'
    var divCloseLayer = document.createElement('div')
    var anchorClickLayer = document.createElement('a')
  
    object_assign(anchorClickLayer.style, {
      cursor: 'pointer',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      width: '100%',
      zIndex: '10'
    })
  
    anchorClickLayer.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL, URL_REDIRECT_LANDING), '_blank')
    })
  
    object_assign(divLayer.style, {
      height: utils.parsePixel(LAYER_HEIGHT),
      left: '0',
      bottom:'0',
      right: '0',
      margin: 'auto',
      position: 'fixed',
      top: '0',
      width: utils.parsePixel(LAYER_WIDTH),
      zIndex: '99999999'
    });
  
    object_assign(divCloseLayer.style, {
      cursor: 'pointer',
      height: utils.parsePixel(BUTTON_CLOSE_HEIGHT),
      position: 'absolute',
      right:'0',
      top: '0',
      width: utils.parsePixel(BUTTON_CLOSE_WIDTH),
      zIndex: '20'
    })
  
    if (BUTTON_CLOSE_ICON !== '') {
      var imgCloseLayer = document.createElement('img')
      imgCloseLayer.src = BUTTON_CLOSE_ICON
      imgCloseLayer.style.width = '100%';
      imgCloseLayer.style.height = '100%';
      divCloseLayer.appendChild(imgCloseLayer)
    }
    
    if (IS_LAYER_HTML) {
      var iframeLayer = document.createElement('iframe')
      iframeLayer.scrolling = 'no'
      iframeLayer.src = LAYER_URL + '?' + CACHEBUSTER
      object_assign(iframeLayer, {
        height: utils.parsePixel(LAYER_HEIGHT),
        width: utils.parsePixel(LAYER_WIDTH)
      })
      object_assign(iframeLayer.style, {
        border: 0
      })
      divLayer.appendChild(iframeLayer)
    } else {
      var imgLayer = document.createElement('img')
      imgLayer.src = LAYER_URL + '?' + CACHEBUSTER;
      imgLayer.style.width = '100%';
      imgLayer.style.height = '100%';
      divLayer.appendChild(imgLayer);
    }
  
    divCloseLayer.addEventListener('click', function () {
      var $divLayer = document.body.ownerDocument.defaultView.parent.document.getElementById('DivLayer')
      $divLayer.parentNode.removeChild($divLayer)
      document.body.appendChild(divBanner)
    })
  
    divLayer.appendChild(anchorClickLayer)
    divLayer.appendChild(divCloseLayer)
    document.body.ownerDocument.defaultView.parent.document.body.appendChild(divLayer)
  
    /* BANNER */
    var divBanner = document.createElement('div')
    var anchorClickBanner = document.createElement('a')
  
    object_assign(anchorClickBanner.style, {
      cursor: 'pointer',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      width: '100%',
      zIndex: '10'
    })
    
    anchorClickBanner.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL, URL_REDIRECT_LANDING), '_blank')
    })
  
    if (IS_BANNER_HTML) {
      var iframeBanner = document.createElement('iframe')
      iframeBanner.scrolling = 'no'
      iframeBanner.src = BANNER_URL + '?' + CACHEBUSTER
      object_assign(iframeBanner, {
        height: utils.parsePixel(CREATIVE_HEIGHT),
        width: utils.parsePixel(CREATIVE_WIDTH)
      })
      object_assign(iframeBanner.style, {
        border: 0
      })
      divBanner.appendChild(iframeBanner)
    } else {
      var imgBanner = document.createElement('img')
      imgBanner.src = BANNER_URL + '?' + CACHEBUSTER
      divBanner.appendChild(imgBanner)
    }
  
    divBanner.appendChild(anchorClickBanner)
  
    function object_assign() {
      var obj = arguments[0]
      for (var index = 1; index < arguments.length; index++) {
        var args = arguments[index]
        for (field in args) {
          obj[field] = args[field]
        }
      }
      return obj
    }
  })()