(function () {

    var
        CACHEBUSTER = '${CACHEBUSTER}',
        IS_DEBUG = '#{IS_DEBUG}' == 'true' ? true : false,
        CREATIVE_ID = '${CREATIVE_ID}',
        CREATIVE_WIDTH = '${CREATIVE_WIDTH}',
        CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}',
        MEDIA_URL = '${MEDIA_URL}',
        MEDIA_URL_AS = '#{MEDIA_URL_AS}',
        MEDIA_URL_TOP = '#{MEDIA_URL_TOP}' != '\#\{MEDIA_URL_TOP\}' ? '#{MEDIA_URL_TOP}' : 'http://#',
        MEDIA_URL_TOP_ALT = '#{MEDIA_URL_TOP_ALT}' != '\#\{MEDIA_URL_TOP_ALT\}' ? '#{MEDIA_URL_TOP_ALT}' : 'http://#',
        MEDIA_URL_RIGHT = '#{MEDIA_URL_RIGHT}' != '\#\{MEDIA_URL_RIGHT\}' ? '#{MEDIA_URL_RIGHT}' : 'http://#',
        MEDIA_URL_RIGHT_ALT = '#{MEDIA_URL_RIGHT_ALT}' != '\#\{MEDIA_URL_RIGHT_ALT\}' ? '#{MEDIA_URL_RIGHT_ALT}' : 'http://#',
        MEDIA_URL_LEFT = '#{MEDIA_URL_LEFT}' != '\#\{MEDIA_URL_LEFT\}' ? '#{MEDIA_URL_LEFT}' : 'http://#',
        MEDIA_URL_LEFT_ALT = '#{MEDIA_URL_LEFT_ALT}' != '\#\{MEDIA_URL_LEFT_ALT\}' ? '#{MEDIA_URL_LEFT_ALT}' : 'http://#',
        CLICK_URL = '${CLICK_URL}',
        MEDIA_URL_TOP_WIDTH = Number('#{MEDIA_URL_TOP_WIDTH}'),
        MEDIA_URL_TOP_HEIGHT = Number('#{MEDIA_URL_TOP_HEIGHT}'),
        MEDIA_URL_TOP_IS_FIXED = '#{MEDIA_URL_TOP_IS_FIXED}' == 'true' ? true : false,
        MEDIA_URL_RIGHT_WIDTH = Number('#{MEDIA_URL_RIGHT_WIDTH}'),
        MEDIA_URL_RIGHT_HEIGHT = Number('#{MEDIA_URL_RIGHT_HEIGHT}'),
        MEDIA_URL_RIGHT_IS_FIXED = '#{MEDIA_URL_RIGHT_IS_FIXED}' == 'true' ? true : false,
        MEDIA_URL_LEFT_WIDTH = Number('#{MEDIA_URL_LEFT_WIDTH}'),
        MEDIA_URL_LEFT_HEIGHT = Number('#{MEDIA_URL_LEFT_HEIGHT}'),
        MEDIA_URL_LEFT_IS_FIXED = '#{MEDIA_URL_LEFT_IS_FIXED}' == 'true' ? true : false,
        BACKGROUND_COLOR = '#{BACKGROUND_COLOR}',
        IS_BACKGROUND_FIXED = '#{IS_BACKGROUND_FIXED}' == 'true' ? true : false,
        IS_INTERATIVE = '#{IS_INTERATIVE}' == 'true' ? true : false;
    IS_DEBUG && console.log('INFO: Debug Enabled for template of creative: ', CREATIVE_ID);

    var
        utils = {
            parse: function (value) {
                return (isNaN(value)) ? value : (value + "px");
            }
        },
        parentDocument = document.body.ownerDocument.defaultView.parent.document,
        parentBody = document.body.ownerDocument.defaultView.parent.document.body,
        currentBody = document.body,
        win = document.body.ownerDocument.defaultView.parent,
        divTop = document.createElement('div'),
        iframeTop = document.createElement('iframe'),
        iframeTopAlt = document.createElement('iframe'),
        divRight = document.createElement('div'),
        iframeRight = document.createElement('iframe'),
        iframeRightAlt = document.createElement('iframe'),
        divLeft = document.createElement('div'),
        iframeLeft = document.createElement('iframe'),
        iframeLeftAlt = document.createElement('iframe'),
        anchorClick = null,
        media_url_top = MEDIA_URL_TOP,
        media_url_right = MEDIA_URL_RIGHT,
        media_url_left = MEDIA_URL_LEFT,
        media_url_top_width = MEDIA_URL_TOP_WIDTH,
        media_url_top_height = MEDIA_URL_TOP_HEIGHT,
        is_alt = false;

    const makeUrlRedirect = function (url, urlredir) {
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    if (MEDIA_URL_AS == 'background') {
        media_url_top = MEDIA_URL_TOP;
    } else if (MEDIA_URL_AS == 'top') {
        media_url_top = MEDIA_URL;
    } else if (MEDIA_URL_AS == 'right') {
        media_url_right = MEDIA_URL;
    } else if (MEDIA_URL_AS == 'left') {
        media_url_left = MEDIA_URL;
    } else if (MEDIA_URL_AS == 'banner') {
        media_url_top = MEDIA_URL;
        media_url_top_width = CREATIVE_WIDTH;
        media_url_top_height = CREATIVE_HEIGHT;
    }

    parentBody.classList.add("tc");

    parentDocument.querySelector(".ads-top") ? parentDocument.querySelector(".ads-top").style.height = CREATIVE_HEIGHT : null
    if (media_url_top != 'http://#') {
        var is_alt = MEDIA_URL_TOP_ALT != 'http://#' && IS_INTERATIVE;
        if (MEDIA_URL_AS == 'banner') {
            currentBody.appendChild(divTop);
            anchorClick = document.createElement('a');
            divTop.appendChild(anchorClick);
            divTop.appendChild(iframeTop);
            is_alt && divTop.appendChild(iframeTopAlt);
            anchorClick.addEventListener('click', function () {
                window.open(makeUrlRedirect(CLICK_URL, MEDIA_URL_TOP_ALT), '_blank');
            });
            object_assign(divTop.style, {
                position: 'relative',
                top: '0px',
                width: utils.parse(media_url_top_width),
                height: utils.parse(media_url_top_height)
            });
        } else {
            parentBody.appendChild(divTop);
            anchorClick = document.createElement('a');
            divTop.appendChild(anchorClick);
            divTop.appendChild(iframeTop);
            is_alt && divTop.appendChild(iframeTopAlt);
            anchorClick.addEventListener('click', function () {
                window.open(makeUrlRedirect(CLICK_URL, MEDIA_URL_TOP_ALT), '_blank');
            });
            object_assign(divTop.style, {
                position: MEDIA_URL_TOP_IS_FIXED ? 'fixed' : 'absolute',
                top: '0px',
                left: utils.parse((win.innerWidth / 2) - (media_url_top_width / 2)),
                width: utils.parse(media_url_top_width),
                height: utils.parse(media_url_top_height)
            });
            win.addEventListener("resize", function () {
                object_assign(divTop.style, {
                    left: utils.parse((win.innerWidth / 2) - (media_url_top_width / 2))
                });
            });
        }

        object_assign(anchorClick.style, {
            position: 'absolute',
            top: '0px',
            left: '0px',
            width: utils.parse(media_url_top_width),
            height: utils.parse(media_url_top_height),
        });
        var
            iframeTopStyle = {
                width: utils.parse(media_url_top_width),
                height: utils.parse(media_url_top_height),
                border: '0px'
            },
            iframeTopAttributes = {
                src: media_url_top,
                width: utils.parse(media_url_top_width),
                height: utils.parse(media_url_top_height),
                tabindex: '-1',
                marginwidth: '0',
                marginheight: '0',
                scrolling: 'no',
                frameborder: '0'
            };
        object_assign(iframeTop.style, iframeTopStyle);
        object_assign(iframeTop, iframeTopAttributes);
        is_alt && object_assign(iframeTopAlt.style, iframeTopStyle, {
            display: 'none'
        });
        is_alt && object_assign(iframeTopAlt, iframeTopAttributes, {
            src: MEDIA_URL_TOP_ALT
        });
        is_alt && iframeAlt(divTop);
    }

    if (media_url_right != 'http://#') {
        var is_alt = MEDIA_URL_RIGHT_ALT != 'http://#' && IS_INTERATIVE;
        parentBody.appendChild(divRight);
        anchorClick = document.createElement('a');
        divRight.appendChild(anchorClick);
        divRight.appendChild(iframeRight);
        is_alt && divRight.appendChild(iframeRightAlt);
        anchorClick.addEventListener('click', function () {
            window.open(makeUrlRedirect(CLICK_URL, MEDIA_URL_RIGHT_ALT), '_blank');
        });
        object_assign(anchorClick.style, {
            position: 'absolute',
            top: '0px',
            left: '0px',
            width: utils.parse(MEDIA_URL_RIGHT_WIDTH),
            height: utils.parse(MEDIA_URL_RIGHT_HEIGHT),
        });
        var
            iframeRightStyle = {
                width: utils.parse(MEDIA_URL_RIGHT_WIDTH),
                height: utils.parse(MEDIA_URL_RIGHT_HEIGHT),
                border: '0px'
            },
            iframeRightAttributes = {
                src: media_url_right,
                width: utils.parse(MEDIA_URL_RIGHT_WIDTH),
                height: utils.parse(MEDIA_URL_RIGHT_HEIGHT),
                tabindex: '-1',
                marginwidth: '0',
                marginheight: '0',
                scrolling: 'no',
                frameborder: '0'
            };
        object_assign(iframeRight.style, iframeRightStyle);
        object_assign(iframeRight, iframeRightAttributes);
        is_alt && object_assign(iframeRightAlt.style, iframeRightStyle, {
            display: 'none'
        });
        is_alt && object_assign(iframeRightAlt, iframeRightAttributes, {
            src: MEDIA_URL_RIGHT_ALT
        });
        is_alt && iframeAlt(divRight);
        object_assign(divRight.style, {
            position: MEDIA_URL_RIGHT_IS_FIXED ? 'fixed' : 'absolute',
            top: '0px',
            right: '0px',
            width: utils.parse(MEDIA_URL_RIGHT_WIDTH),
            height: utils.parse(MEDIA_URL_RIGHT_HEIGHT)
        });
    }

    if (media_url_left != 'http://#') {
        var is_alt = MEDIA_URL_LEFT_ALT != 'http://#' && IS_INTERATIVE;
        parentBody.appendChild(divLeft);
        anchorClick = document.createElement('a');
        divLeft.appendChild(anchorClick);
        divLeft.appendChild(iframeLeft);
        is_alt && divLeft.appendChild(iframeLeftAlt);
        anchorClick.addEventListener('click', function () {
            window.open(makeUrlRedirect(CLICK_URL, MEDIA_URL_LEFT_ALT), '_blank');
        });
        object_assign(anchorClick.style, {
            position: 'absolute',
            top: '0px',
            left: '0px',
            width: utils.parse(MEDIA_URL_LEFT_WIDTH),
            height: utils.parse(MEDIA_URL_LEFT_HEIGHT),
        });
        var
            iframeLeftStyle = {
                width: utils.parse(MEDIA_URL_LEFT_WIDTH),
                height: utils.parse(MEDIA_URL_LEFT_HEIGHT),
                border: '0px'
            },
            iframeLeftAttributes = {
                src: media_url_left,
                width: utils.parse(MEDIA_URL_LEFT_WIDTH),
                height: utils.parse(MEDIA_URL_LEFT_HEIGHT),
                tabindex: '-1',
                marginwidth: '0',
                marginheight: '0',
                scrolling: 'no',
                frameborder: '0'
            };
        object_assign(iframeLeft.style, iframeLeftStyle);
        object_assign(iframeLeft, iframeLeftAttributes);
        is_alt && object_assign(iframeLeftAlt.style, iframeLeftStyle, {
            display: 'none'
        });
        is_alt && object_assign(iframeLeftAlt, iframeLeftAttributes, {
            src: MEDIA_URL_LEFT_ALT
        });
        is_alt && iframeAlt(divLeft);
        object_assign(divLeft.style, {
            position: MEDIA_URL_LEFT_IS_FIXED ? 'fixed' : 'absolute',
            top: '0px',
            left: '0px',
            width: utils.parse(MEDIA_URL_LEFT_WIDTH),
            height: utils.parse(MEDIA_URL_LEFT_HEIGHT)
        });
    }

    if (MEDIA_URL_AS == 'background') {
        IS_DEBUG && console.log('TEMPLATE LOG: ', 'MEDIA_URL_AS', MEDIA_URL_AS);
        object_assign(parentBody.style, {
            backgroundColor: BACKGROUND_COLOR,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center top',
            backgroundImage: 'url(' + MEDIA_URL + ')',
            backgroundAttachment: IS_BACKGROUND_FIXED ? 'fixed' : 'unset'
        });
        IS_DEBUG && console.log('TEMPLATE LOG: ', 'parentBody:', [parentBody]);
        parentBody.addEventListener('click', function (event) {
            IS_DEBUG && console.log('TEMPLATE LOG: ', 'clicktag:', [event.srcElement], 'event:', [event]);
            if (parentBody == event.srcElement) {
                window.open(CLICK_URL);
            }
        });
    }
    if (BACKGROUND_COLOR) {
        object_assign(parentBody.style, {
            backgroundColor: BACKGROUND_COLOR
        });
    }

    var eventNames = [
        'mouseenter',
        'mouseout',
        'mouseover',
        'mouseleave',
        'click'
    ];
    var iframes = [iframeTop, iframeLeft, iframeRight];
    for (var indexElement = 0; indexElement < iframes.length; indexElement++) {
        var element = iframes[indexElement].parentElement;
        for (var indexEvent = 0; indexEvent < eventNames.length; indexEvent++) {
            var eventName = eventNames[indexEvent];
            element && element.addEventListener(eventName, function (event) {

                for (var indexFrameElement = 0; indexFrameElement < iframes.length; indexFrameElement++) {
                    var
                        iframe = iframes[indexFrameElement],
                        data = {
                            action: 'adnx.template.event.' + event.type
                        };
                    IS_DEBUG && console.log('TEMPLATE LOG: ', 'postMessage', {
                        iframe: iframe,
                        data: data
                    });
                    iframe && iframe.contentWindow.postMessage(data, '*');
                }

            });
        }
    }

    function iframeAlt(element) {
        element.addEventListener('mouseenter', function () {
            object_assign(iframeTop.style, {
                display: 'none'
            });
            object_assign(iframeTopAlt.style, {
                display: 'block'
            });
            object_assign(iframeRight.style, {
                display: 'none'
            });
            object_assign(iframeRightAlt.style, {
                display: 'block'
            });
            object_assign(iframeLeft.style, {
                display: 'none'
            });
            object_assign(iframeLeftAlt.style, {
                display: 'block'
            });
        });
        element.addEventListener('mouseout', function () {
            object_assign(iframeTop.style, {
                display: 'block'
            });
            object_assign(iframeTopAlt.style, {
                display: 'none'
            });
            object_assign(iframeRight.style, {
                display: 'block'
            });
            object_assign(iframeRightAlt.style, {
                display: 'none'
            });
            object_assign(iframeLeft.style, {
                display: 'block'
            });
            object_assign(iframeLeftAlt.style, {
                display: 'none'
            });
        });
    }

    function object_assign() {
        var obj = arguments[0];
        for (var index = 1; index < arguments.length; index++) {
            var args = arguments[index];
            for (field in args) {
                obj[field] = args[field];
            }
        }
        return obj;
    };

})();