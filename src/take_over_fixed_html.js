(() => {
    var CLICK_URL = '${CLICK_URL}'
    var MEDIA_URL = '${MEDIA_URL}'
    var CREATIVE_WIDTH = '${CREATIVE_WIDTH}'
    var CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}'
    var CACHEBUSTER = '${CACHEBUSTER}'
    var URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}'
    var TYPE_CREATIVE = '#{TYPE_CREATIVE}'
    var FILE_SMALL_CREATIVE  = '#{FILE_SMALL_CREATIVE}'
    var FILE_BIG_CREATIVE  = '#{FILE_BIG_CREATIVE}'
    var CLOSE_BUTTON = '#{CLOSE_BUTTON}'
    var CREATIVE_WIDTH_BIG = '#{CREATIVE_WIDTH_BIG}'
    var CREATIVE_HEIGHT_BIG = '#{CREATIVE_HEIGHT_BIG}'
    var TIME = parseInt('#{TIME}')


    const makeUrlRedirect = function (url, urlredir) {
        urlredir = url + urlredir;
        return urlredir;
    }

    const setAnchorTo = function (divToappend, anchor_click, URL) {
        divToappend.appendChild(anchor_click);
        anchor_click.addEventListener("click", function () {
            window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT), '_blank')
        })
    }
    const object_assign = function () {
        var obj = arguments[0], field;
        for (var index = 1; index < arguments.length; index++) {
            var args = arguments[index]
            for (field in args) {
                obj[field] = args[field]
            }
        }
        return obj
    }
    const makeMediaTag = function (EXTENSION, URL_SRC) {
        var mediaTag;
        switch (EXTENSION) {
            case 'image/gif/png':
                mediaTag = document.createElement('img');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                break;
            case 'HTML':
                mediaTag = document.createElement('iframe');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                mediaTag.width = '100%';
                mediaTag.height = '100%';
                mediaTag.scrolling = 'no';
                mediaTag.frameborder = 'no';
                mediaTag.style.border = '0';
                break;
            default:
                alert("The template" + CREATIVE_ID + 'doesnt suppot this format')
        }
        return mediaTag;
    }


    var windowParent = document.body.ownerDocument.defaultView.parent
    var parentDocument = document.body.ownerDocument.defaultView.parent.document
    var div_parent_small = document.createElement('div')
    var tag_small_creative = makeMediaTag(TYPE_CREATIVE, FILE_SMALL_CREATIVE)
    var anchor_click_small = document.createElement('a')
    var div_button_close_icon = document.createElement('div')
    var div_firstparent_big = document.createElement('div')
    var div_parent_big = document.createElement('div')
    var tag_big_creative = makeMediaTag(TYPE_CREATIVE, FILE_BIG_CREATIVE)
    var anchor_click_big = document.createElement('a')

    var CURRENT_SCROLL = 0
    var IS_CLOSE = false

    const appendSmallCreative = function () {
        div_parent_small.appendChild(tag_small_creative);
        setAnchorTo(div_parent_small, anchor_click_small, URL_LANDING_REDIRECT)
        document.body.appendChild(div_parent_small)
    }
    const closeTakeOver = function () {
        var set;
        set = setInterval(function () {
            windowParent.scrollTo(CURRENT_SCROLL - 0.1, 0)
            CURRENT_SCROLL = CURRENT_SCROLL - 0.1;
            CURRENT_SCROLL > 0 ? closeTakeOver() : clearInterval(set)
        }, 2)

    }
    parentDocument.body.style.cssText += 'overflow-x: hidden !important'
    object_assign(div_parent_small.style, {
        width: CREATIVE_WIDTH + 'px',
        height: CREATIVE_HEIGHT + 'px',
        position: 'relative'
    });

    object_assign(tag_small_creative.style, {
        width: '100%',
        height: '100%'
    });

    object_assign(anchor_click_small.style, {
        position: 'absolute',
        width: '100%',
        height: '100%',
        cursor: 'pointer',
        top: '0',
        left: '0',
        zIndex: '2'
    });

    object_assign(div_parent_big.style, {
        position: 'absolute',
        width: '100%',
        height: '100vh',
        left: '100%',
        top: '0',
        zIndex: '999999999999999999',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white'
    });
    object_assign(anchor_click_big.style, {
        position: 'absolute',
        width: '100%',
        height: '100%',
        cursor: 'pointer',
        top: '0',
        left: '0',
        zIndex: '5'
    });


    object_assign(div_button_close_icon.style,{
        width: '60px',
        height: '60px',
        position: 'absolute',
        zIndex: '999',
        top: '0',
        right: '0',
        backgroundImage: 'url('+CLOSE_BUTTON+')',
        backgroundSize: 'contain',
        cursor: 'pointer'
    })
    object_assign(div_firstparent_big.style, {
        width: CREATIVE_WIDTH_BIG + 'px',
        height: CREATIVE_HEIGHT_BIG + 'px',
        position:'relative'
    })
    object_assign(tag_big_creative.style, {
        width: '100%',
        height: '100%',
    });

    div_firstparent_big.appendChild(tag_big_creative)
    div_firstparent_big.appendChild(div_button_close_icon)
    div_parent_big.appendChild(div_firstparent_big)
    setAnchorTo(div_parent_big, anchor_click_big, URL_LANDING_REDIRECT)
    parentDocument.body.insertBefore(div_parent_big, parentDocument.body.childNodes[0])

    setTimeout(function () {
        windowParent.scrollTo(windowParent.parent.innerWidth, 0)
        CURRENT_SCROLL = windowParent.scrollX
        parentDocument.body.style.cssText += 'overflow-y: hidden !important'
    }, 1000)

    setTimeout(function () {
        if(IS_CLOSE == false){
            appendSmallCreative()
            closeTakeOver()
            parentDocument.body.style.overflowY = ""
        }
    }, TIME * 1000)


    div_button_close_icon.addEventListener('click', function(){
        appendSmallCreative()
        closeTakeOver()
        parentDocument.body.style.overflowY = ""
        IS_CLOSE = true
    })
})()