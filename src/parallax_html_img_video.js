(function(){
    var
    CACHEBUSTER = '${CACHEBUSTER}',
    CREATIVE_ID = '${CREATIVE_ID}',
    CREATIVE_WIDTH = '#{CREATIVE_WIDTH}',
    CREATIVE_HEIGHT = '#{CREATIVE_HEIGHT}',
    MEDIA_URL = '${MEDIA_URL}',
    CLICK_URL = '${CLICK_URL}',
    USE_LANDING_REDIRECT = '#{USE_LANDING_REDIRECT}' == 'true' ? true : false,
    URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}',
    MEDIA_MATERIAL = '#{MEDIA_MATERIAL}',
    EXTENSION_TYPE = '#{EXTENSION_TYPE}',
    FORMAT_MEDIA = '#{FORMAT_MEDIA}',
    PLAY_ICON = '#{PLAY_ICON}',
    VOL_ICON_ON = '#{VOL_ICON_ON}',
    VOL_ICON_OFF = '#{VOL_ICON_OFF}',
    ratioAspect = CREATIVE_WIDTH/CREATIVE_HEIGHT,
    ratioAspectIf = CREATIVE_HEIGHT/CREATIVE_WIDTH,
    winParent = document.body.ownerDocument.defaultView.parent,
    IS_MOBILE = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)|(Windows Phone)/i) ? true : false,
    portal,
    portales = [
        ['trome', '600', '85', '105'] ,//portal , max-width inline, height of navbar used as top , mobil top
        ['depor', '620', '170', '88'],
        ['gestion', '620', '45', '35'],//
        ['elcomercio', '620', '45', '25'],//
        ['peru.com', '620', '80', '0'],//
        ['peru21', '620', '60', '43'],//
        ['publimetro', '620', '45', '75'],//
        ['diariocorreo', '520', '45', '75'],//
        ['elbocon', '520', '45', '40'],
        ['elshow', '520', '45', '0'],//
        ['mujerpandora', '620', '45', '64'],//
        ['ojo', '520', '45', '0']//
    ],
    URL_PORTAL = location.host,
    topToDesktop,
    topToMobile;

    portales.forEach(function(portal, i){
        if(URL_PORTAL.search(portal[0])>=0){
            if(IS_MOBILE){
                CREATIVE_WIDTH = winParent.outerWidth;
                topToMobile = portal[3];
                
            }else{   
                CREATIVE_WIDTH = portal[1];
                topToDesktop = portal[2];
                
            }
        }
    })
    

    //helpers
    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    const object_assign = function() {
        var obj = arguments[0]
        for (var index = 1; index < arguments.length; index++) {
            var args = arguments[index]
            for (field in args) {
                obj[field] = args[field]
            }
        }
        return obj
    }
    const setAnchorTo = function(divToappend, anchor_click){
        divToappend.appendChild(anchor_click);
        anchor_click.addEventListener("click", function(){
          window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT),'_blank')
        })
    }
    const  parse = function(val){
        return (isNaN(val))?val:(val + "px");
    }
    const calcHeight = function(size){
        return (parseInt(size) - 20) + 'px';
    }
  
    //make tags
    var parentBody = document.body.ownerDocument.defaultView.parent.document.body,
        parentDocument = document.body.ownerDocument.defaultView.parent.document,
        div = document.createElement('div'),
        anchorClick = document.createElement('a'),
        advertisementDiv = document.createElement('span'),
        playIconA = document.createElement('a'),
        volIconA = document.createElement('a'),
        videoBackgroung = document.createElement('video');
        div.id = 'parallaxTemplate',
        selector = IS_MOBILE ? 'm_movil3' : 'd_inline';
        if(EXTENSION_TYPE == 'image/gif/png' || EXTENSION_TYPE == 'HTML' && FORMAT_MEDIA == 'vertical video'){
            if((( (IS_MOBILE ? winParent.outerHeight : winParent.innerHeight) - (topToDesktop || topToMobile)) * ratioAspect) <= parseInt(CREATIVE_WIDTH)){
                CREATIVE_HEIGHT = (IS_MOBILE ? winParent.outerHeight : winParent.innerHeight) - (topToDesktop || topToMobile);
                CREATIVE_WIDTH = CREATIVE_HEIGHT * ratioAspect;
                
            }else{
                CREATIVE_HEIGHT = CREATIVE_WIDTH * ratioAspectIf;
                CREATIVE_WIDTH = CREATIVE_WIDTH;//referencia
                
            }
        }else if(EXTENSION_TYPE == 'video' && FORMAT_MEDIA == 'video + backgound'){
            //if(!IS_MOBILE){
                CREATIVE_HEIGHT = CREATIVE_WIDTH * ratioAspectIf;
                CREATIVE_WIDTH = CREATIVE_WIDTH;//referencia
                
            //}
        }

    parentBody.style.overflowX = 'hidden';
    advertisementDiv.innerText  = 'Advertisement';
    object_assign(playIconA.style, {
        position: 'relative',
        cursor: 'pointer',
        fontSize: '78px',
        position: 'absolute',
        top: '50%',
        left: '0',
        right: '0',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '-59px',
        width: '105px',
        height: '105px',
        zIndex: '11',
        backgroundImage : 'url("'+PLAY_ICON+'")',
        backgroundSize : '100% 100%',
        display: IS_MOBILE ? 'block' : 'none'
    })
    object_assign(anchorClick.style, {
        cursor: 'pointer',
        height: IS_MOBILE ? winParent.innerHeight +'px': parse(CREATIVE_HEIGHT),
        left: '0',
        position: 'absolute',
        top: '0',
        width: IS_MOBILE ? winParent.innerWidth + 'px' : parse(CREATIVE_WIDTH),
        zIndex: '10'
    })
    object_assign(advertisementDiv.style, {
        position: 'absolute',
        left: '0',
        width: '100%',
        textAlign: 'center',
        fontFamily: 'Arial,sans-serif',
        backgroundColor: '#000000',
        fontSize: '12px',
        color: '#ffffff',
        webkitTransform: 'translateZ(0)',
        transform: 'translateZ(0)',
        top: FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? topToMobile + 'px' : '0'
    })
    object_assign(volIconA.style, {
        display: IS_MOBILE ? 'none' : 'block',
        cursor: 'pointer',
        position: 'absolute',
        bottom: '12px',
        right: '25px',
        width: '36px',
        height: '36px',
        fontSize: '2em',
        lineHeight: '1em',
        zIndex: '11',
        backgroundImage : 'url("'+VOL_ICON_OFF+'")',
        backgroundSize: '100% 100%'
    })
    if(IS_MOBILE){
        volIconA.style.bottom = '';
        volIconA.style.top = (parseInt(topToMobile) + 36) + 'px'
    }
    advertisementDiv.style.setProperty('-moz-transform', 'translateZ(0)');
    advertisementDiv.style.setProperty('-ms-transform', 'translateZ(0)');
    advertisementDiv.style.setProperty('-o-transform', 'translateZ(0)');
    object_assign(div.style, {
        height: IS_MOBILE ? winParent.outerHeight + 'px' : parse(CREATIVE_HEIGHT),
        width: IS_MOBILE ? winParent.outerWidth + 'px' : parse(CREATIVE_WIDTH),
        position: 'relative'
    });

    videoBackgroung.width = IS_MOBILE ? 1200 : winParent.innerWidth;
    videoBackgroung.height = IS_MOBILE ?  800 : winParent.innerHeight;
    videoBackgroung.id = 'videoBackGround';
    object_assign(videoBackgroung.style, {
        position: 'fixed',
        top: '0',
        left: '-351px',
        filter: 'blur(7px) grayscale(0%) invert(0%) sepia(0%)',
        transform: 'translateZ(0)'
    });
    videoBackgroung.style.setProperty('-moz-transform', 'translateZ(0)');
    videoBackgroung.style.setProperty('-ms-transform', 'translateZ(0)');
    videoBackgroung.style.setProperty('-o-transform', 'translateZ(0)');
    videoBackgroung.src = MEDIA_MATERIAL + '?' + CACHEBUSTER;
    videoBackgroung.controls = false;
    videoBackgroung.muted = true;
    videoBackgroung.autoplay = IS_MOBILE ? false : true;
   
    switch (EXTENSION_TYPE) {
        case 'image/gif/png':
            var imgTag = document.createElement('img');
            imgTag.src = MEDIA_MATERIAL+ '?' + CACHEBUSTER;
            object_assign(imgTag.style, {
                height:FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight + 'px' : parse(CREATIVE_HEIGHT),
                width: FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerWidth + 'px' : parse(CREATIVE_WIDTH)
            })
            div.appendChild(imgTag);
            break;
        case 'video': 
            var widthVideo = FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerWidth : CREATIVE_WIDTH,
                heightVideo = FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight : CREATIVE_HEIGHT;
            var videoTag = document.createElement('video');
            videoTag.width = widthVideo;
            videoTag.height = heightVideo;
            videoTag.src = MEDIA_MATERIAL + '?' + CACHEBUSTER;
            videoTag.controls = false;
            videoTag.muted = true;
            videoTag.autoplay = IS_MOBILE ? false : true;
            videoTag.volume = 0.0;
            div.appendChild(videoTag);
            break;
        case 'HTML':
            var iframeTag = document.createElement('iframe');
            iframeTag.src = MEDIA_MATERIAL + '?' + CACHEBUSTER;
            iframeTag.width = FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerWidth + 'px': parse(CREATIVE_WIDTH);
            iframeTag.height = FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight + 'px': parse(CREATIVE_HEIGHT);
            iframeTag.scrolling = 'no';
            iframeTag.frameborder = '0';
            div.appendChild(iframeTag);
            break
        default:
            console.log("The template" + CREATIVE_ID + 'doesnt suppot this format')
    }
    var intervalStatus;
    const videoStatus = function(){
        var videoStatus = div.firstChild;
        intervalStatus = setInterval(function(){
            var timeRest = Math.round(videoStatus.duration) - Math.round(videoStatus.currentTime);
            if(timeRest == 0){
                playIconA.style.display = 'block';
                volIconA.style.display = 'none';
                clearInterval(intervalStatus);
            }else if(videoStatus.paused){
                playIconA.style.display = 'block';
                volIconA.style.display = 'none';
            }else if(videoStatus.paused == false){
                playIconA.style.display = 'none';
                volIconA.style.display = 'block';
            }
        }, 1000)
    }
    playIconA.addEventListener('click', function(){
        document.querySelector('#parallaxTemplate video').play();
        FORMAT_MEDIA == 'video + backgound' ? videoBackgroung.play() : null
        this.style.display = 'none';
        volIconA.style.display = 'block';
        videoStatus();
    });
    var counterVol = 2; 
    volIconA.addEventListener('click', function(){
        if(counterVol % 2 == 0){ 
            this.parentElement.firstChild.volume = 1.0;
            this.parentElement.firstChild.muted = false;
            this.style.backgroundImage = 'url("'+VOL_ICON_ON+'")';
        }else{
            this.parentElement.firstChild.volume = 0.0;
            this.parentElement.firstChild.muted = true;
            this.style.backgroundImage = 'url("'+VOL_ICON_OFF+'")';
        }
        counterVol++;
    });
    USE_LANDING_REDIRECT ? setAnchorTo(div, anchorClick) : null;
    div.appendChild(advertisementDiv);
    if(EXTENSION_TYPE == 'video'){
        div.appendChild(playIconA);
        div.appendChild(volIconA);
    }
    document.body.appendChild(div);
    videoStatus();
    var placement = parentDocument.querySelector('#ads_'+selector),
        placement_firstchild = parentDocument.querySelector('#div_utif_ads_'+selector),
        placement_firstchild_iframe = parentDocument.querySelector('#div_utif_ads_'+selector+' iframe');
  
    if(placement && placement_firstchild && placement_firstchild_iframe){ 
        object_assign(placement.style, {
            height: FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight + 'px': parse(CREATIVE_HEIGHT),
            position: 'relative',
            width: /*FORMAT_MEDIA == 'video + backgound' &&*/ IS_MOBILE ? '100%' : parse(CREATIVE_WIDTH),
            
        })

        object_assign(placement_firstchild.style, {
            overflow: 'hidden',
            position: 'absolute',
            width:  /*FORMAT_MEDIA == 'video + backgound' &&*/ IS_MOBILE ? '100%': parse(CREATIVE_WIDTH),//
            height: FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight + 'px': parse(CREATIVE_HEIGHT),
            clip: 'rect(auto, auto, auto, auto)'
        })

        topToDesktop = FORMAT_MEDIA == 'video + backgound' ? (winParent.innerHeight/2 - CREATIVE_HEIGHT/2) : topToDesktop;
        console.log(FORMAT_MEDIA == 'video + backgound' ?  IS_MOBILE ? '0' : topToDesktop :  IS_MOBILE ? topToMobile : topToDesktop);
        object_assign(placement_firstchild_iframe.style, {
            border: '0px',
            overflow: 'hidden',
            display: 'inline-block',
            position: 'fixed',
            zIndex:  IS_MOBILE ? '2' : '11000',
            background: FORMAT_MEDIA == 'video + backgound' ? '' : 'rgb(255, 255, 255)',
            top:  (FORMAT_MEDIA == 'video + backgound' ?  IS_MOBILE ? '0' : topToDesktop :  IS_MOBILE ? topToMobile : topToDesktop) + 'px',
            width:  FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerWidth + 'px': parse(CREATIVE_WIDTH),
            height: FORMAT_MEDIA == 'video + backgound' && IS_MOBILE ? winParent.outerHeight + 'px': parse(CREATIVE_HEIGHT),
            bottom: '0'//
        })
        
        IS_MOBILE ? 
            object_assign(placement_firstchild_iframe.style, {
                left :'0',
                right : '0',
            }) : null;
        if(EXTENSION_TYPE == 'image/gif/png' || EXTENSION_TYPE == 'HTML' && FORMAT_MEDIA == 'vertical video'){
            placement_firstchild_iframe.style.margin = 'auto';
            placement_firstchild.style.display = 'flex';
            placement_firstchild.style.justifyContent = 'center';
            placement_firstchild.style.alignItems = 'center';
            placement.style.margin= 'auto'
            
        }
        FORMAT_MEDIA == 'video + backgound' ? placement_firstchild.appendChild(videoBackgroung) : null;
    }
})()