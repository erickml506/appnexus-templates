(function(){
    var
    CACHEBUSTER = '${CACHEBUSTER}',
    CREATIVE_ID = '${CREATIVE_ID}',
    MEDIA_URL = '${MEDIA_URL}',
    CREATIVE_WIDTH = '${CREATIVE_WIDTH}',
    CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}',
    CLICK_URL = '${CLICK_URL}',
    EXTENSION_TYPE = '#{EXTENSION_TYPE}',
    URL_LANDING_REDIRECT_FIRST = '#{URL_LANDING_REDIRECT_FIRST}',
    URL_LANDING_REDIRECT_SECOND = '#{URL_LANDING_REDIRECT_SECOND}',
    FIRST_CREATIVE = '#{FIRST_CREATIVE}',
    SECOND_CREATIVE = '#{SECOND_CREATIVE}',
    ICON_SCROLLUP = '#{ICON_SCROLLUP}',
    ICON_SCROLLDOWN = '#{ICON_SCROLLDOWN}',
    PLACEMENT_NAME = '#{PLACEMENT_NAME}';

    /*
    |__________________
    |  HELPERS
    |
    */
   const getAbsolutePosition = function(el, doc){
        var yPos = 0;
        while (el) {
          if (el.tagName == "BODY") {
            var yScroll = el.scrollTop || doc.documentElement.scrollTop;
            yPos += (el.offsetTop - yScroll + el.clientTop);
          } else {
            yPos += (el.offsetTop - el.scrollTop + el.clientTop);
          }
       
          el = el.offsetParent;
        }
        return yPos
       
   }
    const makeUrlRedirect = function(url, urlredir){
        //urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        urlredir = url+urlredir;
        return urlredir;
    }

    const setAnchorTo = function(divToappend, anchor_click, URL_LANDING_REDIRECT){
        divToappend.appendChild(anchor_click);
        anchor_click.addEventListener("click", function(){
          window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT),'_blank')
        })
    }
    const scrollTo = function(el1, el2, action){
        if(action == 'scrollUp'){
            el1.style.top = '0';
            el2.style.top = '600px';
        }else{
            el1.style.top = '-600px';
            el2.style.top = '0';
        }
    }
    const object_assign = function() {
        var obj = arguments[0]
        for (var index = 1; index < arguments.length; index++) {
            var args = arguments[index]
            for (field in args) {
                obj[field] = args[field]
            }
        }
        return obj
    }
    const makeMediaTag = function(EXTENSION, URL_SRC){      
        var mediaTag;
        switch (EXTENSION) {
            case 'image/gif/png':
                mediaTag = document.createElement('img');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                break;
            case 'HTML':
                mediaTag = document.createElement('iframe');
                mediaTag.src = URL_SRC + '?' + CACHEBUSTER;
                mediaTag.width = '100%';
                mediaTag.height = '100%';
                mediaTag.scrolling = 'no';
                mediaTag.frameborder = 'no';
                mediaTag.style.border = '0';
            default:
                console.log("The template" + CREATIVE_ID + 'doesnt suppot this format')
        }
        return mediaTag;
    }

     /*
    |__________________
    |  TAGS AND VARS
    |
    */
    var parentDocument = document.body.ownerDocument.defaultView.parent.document,
        placement  = parentDocument.querySelector('#div_utif_ads_d_'+PLACEMENT_NAME),
        iframe  = parentDocument.querySelector('#div_utif_ads_d_'+PLACEMENT_NAME+' iframe'),
        div_parent = document.createElement('div'),
        div_parent_first_creative = document.createElement('div'),
        div_parent_second_creative = document.createElement('div'),
        tag_first_creative = makeMediaTag(EXTENSION_TYPE, FIRST_CREATIVE),
        tag_second_creative = makeMediaTag(EXTENSION_TYPE, SECOND_CREATIVE),
        anchor_click_first = document.createElement('a'),
        anchor_click_second = document.createElement('a'),
        div_parent_icon_up = document.createElement('div'),
        a_icon_scrollup = document.createElement('a'),
        div_parent_icon_down = document.createElement('div'),
        a_icon_scrolldown = document.createElement('a'),
        appendTo = placement || document.body;
    


    /*
    |__________________
    |  SET STYLES
    |
    */
    if(iframe){
        iframe.width = '0px';
        iframe.height = '0px';
    }
    
    object_assign(div_parent.style, {
        width: CREATIVE_WIDTH + 'px',
        height: CREATIVE_HEIGHT + 'px',
        position : 'relative',
        overflowY : 'hidden'
    });

    object_assign(div_parent_first_creative.style, {
        width: CREATIVE_WIDTH + 'px',
        height: CREATIVE_HEIGHT + 'px',
        position: 'absolute',
        top: '0',
        transition: 'top 0.5s'
    });
    object_assign(div_parent_second_creative.style, {
        width: CREATIVE_WIDTH + 'px',
        height: CREATIVE_HEIGHT + 'px',
        position: 'absolute',
        top: '600px',
        transition: 'top 0.5s'
    });
    [tag_first_creative, tag_second_creative, anchor_click_first, anchor_click_second].forEach(function(el, i){
        el.style.width = '100%';
        el.style.height = '100%';
        if(i>1){
            el.style.position = 'absolute';
            el.style.top = '0';
            el.style.left = '0';
            el.style.zIndex = '2';
            el.style.cursor = 'pointer';
        }
    })
    object_assign(div_parent_icon_up.style, {
        position: 'absolute',
        width: '100%',
        height: '35px',
        top: '10px',
        zIndex: '3',
        cursor: 'pointer'
    });
    object_assign(a_icon_scrollup.style, {
        position: 'absolute',
        width: '35px',
        height: '35px',
        left: '0',
        right: '0',
        backgroundImage: 'url('+ICON_SCROLLUP+')',
        backgroundSize: 'cover',
        zIndex: '4',
        margin: 'auto',
        cursor: 'pointer'
    });
    object_assign(div_parent_icon_down.style, {
        position: 'absolute',
        width: '100%',
        height: '35px',
        bottom: '10px',
        zIndex: '3',
        cursor: 'pointer'
    });
    object_assign(a_icon_scrolldown.style, {
        position: 'absolute',
        width: '35px',
        height: '35px',
        left: '0',
        right: '0',
        backgroundImage: 'url('+ICON_SCROLLDOWN+')',
        backgroundSize: 'cover',
        zIndex: '4',
        margin: 'auto',
        cursor: 'pointer'
    });
       /*
    |__________________
    |  SET EVENTS
    |
    */
    var mouseY, el1Top = 0, el2Top = 600, top = 0;
    div_parent.addEventListener('mousemove', function(e){
        mouseY = e.clientY || e.pageY;
        top = getAbsolutePosition(this, parentDocument);
        mouseTop = mouseY - top ;
        //console.log(mouseY, top, mouseTop);
        if(mouseTop > 450 && mouseTop < 555){
            scrollTo(div_parent_first_creative, div_parent_second_creative, 'scrollDown')
        }else if (mouseTop > 45 && mouseTop < 150 ){
            scrollTo(div_parent_first_creative, div_parent_second_creative, 'scrollUp');
        }else if(mouseTop > 555 || mouseTop < 45){
            void 0;
        }else{
            div_parent_first_creative.style.top = (el1Top - mouseTop) + 'px';
            div_parent_second_creative.style.top = (el2Top - mouseTop) + 'px';
        }
       
    });

    [div_parent_icon_up, div_parent_icon_down].forEach(function(el, i){
        el.addEventListener('click', function(){
            i == 0 ? scrollTo(div_parent_first_creative, div_parent_second_creative, 'scrollUp') :  scrollTo(div_parent_first_creative, div_parent_second_creative, 'scrollDown');
            console.log('click');
        });
    });


    /*
    |__________________
    |  APPEND CHILDS
    |
    */
   div_parent_first_creative.appendChild(tag_first_creative);
   setAnchorTo(div_parent_first_creative, anchor_click_first, URL_LANDING_REDIRECT_FIRST);
   div_parent_second_creative.appendChild(tag_second_creative);
   setAnchorTo(div_parent_second_creative, anchor_click_second, URL_LANDING_REDIRECT_SECOND);
   div_parent_icon_up.appendChild(a_icon_scrollup);
   div_parent.appendChild(div_parent_icon_up);
   div_parent.appendChild(div_parent_first_creative);
   div_parent.appendChild(div_parent_second_creative);
   div_parent_icon_down.appendChild(a_icon_scrolldown);
   div_parent.appendChild(div_parent_icon_down);
   appendTo.appendChild(div_parent);
})()