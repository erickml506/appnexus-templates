
(function() {

  var
  CACHEBUSTER = '${CACHEBUSTER}',
  CREATIVE_ID = '${CREATIVE_ID}',
  CLICK_URL = '${CLICK_URL}',
  MEDIA_URL = '${MEDIA_URL}',
  IS_DEBUG = '#{IS_DEBUG}' == 'true'?true:false,
  IS_IMAGE_FORMAT = '#{IS_IMAGE_FORMAT}' == 'true'?true:false,
  BUTTON_CLOSE = '#{BUTTON_CLOSE}' != '\#\{BUTTON_CLOSE\}'?'#{BUTTON_CLOSE}':'http://#',
  BUTTON_CLOSE_DISABLED = '#{BUTTON_CLOSE_DISABLED}' == 'true'?true:false,
  BUTTON_CLOSE_WIDTH = Number('#{BUTTON_CLOSE_WIDTH}'),
  BUTTON_CLOSE_HEIGHT = Number('#{BUTTON_CLOSE_HEIGHT}'),
  BUTTON_CLOSE_RIGHT = Number('#{BUTTON_CLOSE_RIGHT}'),
  BUTTON_CLOSE_TOP = Number('#{BUTTON_CLOSE_TOP}'),
  POSITION = '#{POSITION}',
  LANDING_URL = '#{LANDING_URL}',
 
  INTERSTITIAL_WIDTH = Number('#{INTERSTITIAL_WIDTH}'),
  INTERSTITIAL_HEIGHT = Number('#{INTERSTITIAL_HEIGHT}'),
  IN_LAYER = '#{IN_LAYER}' == 'true'?true:false,
  LAYER_COLOR = '#{LAYER_COLOR}',
  TIME = Number('#{TIME}'),
  POSITION_AS = '#{POSITION_AS}',
  RESIZE_TO_SCREEN = '#{RESIZE_TO_SCREEN}',
  MEDIA_URL = '#{MEDIA_URL}',
  Z_INDEX = Number('#{Z_INDEX}');

  IS_DEBUG && console.log('INFO: Debug Enabled for template of creative: ', CREATIVE_ID);
  const makeUrlRedirect = function(url, urlredir){
    urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
    return urlredir;
  }
  
  var
  utils = {
    parse: function(value){
      return (isNaN(value))?value:(value + "px");
    }
  },
  parentDocument = document.body.ownerDocument.defaultView.parent.document,
  IS_MOBILE = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)|(Windows Phone)/i) ? true : false,
  iframe  = IS_MOBILE ? parentDocument.querySelector('#ads_m_expandible iframe') : parentDocument.querySelector('#ads_d_expandible iframe'),
  parentBody = document.body.ownerDocument.defaultView.parent.document.body,
  currentBody = document.body,
  win = document.body.ownerDocument.defaultView.parent,
  divFloat = document.createElement('div'),
  divClose = document.createElement('div'),
  anchorClick = document.createElement('a'),
  iframeContent = document.createElement('iframe'),
  imageContent = document.createElement('img'),
  container = divFloat;

  if(iframe){
    iframe.width = '0px';
    iframe.height = '0px';
  }
  object_assign(divFloat.style, {
    position: POSITION || 'absolute',
    zIndex: Z_INDEX
  });
  anchorClick.addEventListener('click', function() {
    window.open(makeUrlRedirect(CLICK_URL,LANDING_URL), '_blank');
  });
  object_assign(anchorClick.style, {
    position: 'absolute',
    top: '0px',
    left: '0px'
  });
  if (!BUTTON_CLOSE_DISABLED) {
    if (BUTTON_CLOSE != 'http://#') {
      var imgClose = document.createElement('img');
      imgClose.src = BUTTON_CLOSE;
      object_assign(imgClose.style, {
        position: 'absolute',
        right: '0px',
        top: '0px',
        width:'100%',
        height:'100%'
      });
      divClose.appendChild(imgClose);
    }
    divClose.addEventListener('click', function() {
      container.style.display = 'none';
    });
    object_assign(divClose.style, {
      width: utils.parse(BUTTON_CLOSE_WIDTH),
      height: utils.parse(BUTTON_CLOSE_HEIGHT),
      right: utils.parse(BUTTON_CLOSE_RIGHT),
      top: utils.parse(BUTTON_CLOSE_TOP),
      position: 'absolute',
      cursor: 'pointer',
      zIndex: Z_INDEX+1
    });
    divFloat.appendChild(divClose);
  }
  divFloat.appendChild(anchorClick);
  if (IS_IMAGE_FORMAT) {
    imageContent.src = MEDIA_URL;
    divFloat.appendChild(imageContent);
  }
  else {
    iframeContent.src = MEDIA_URL;
    object_assign(iframeContent.style, {
      border: '0px'
    });
    object_assign(iframeContent, {
      tabindex: '-1',
      marginwidth: '0',
      marginheight: '0',
      scrolling: 'no',
      frameborder: '0'
    });
    divFloat.appendChild(iframeContent);
  }

  if (IN_LAYER) {
    divLayer = document.createElement('div');
    container = divLayer;
    object_assign(divLayer.style, {
      display: 'block',
      position: 'fixed',
      zIndex: Z_INDEX,
      left: 0,
      top: 0,
      width: '100%',
      height: '100%',
      overflow: 'auto',
      backgroundColor: LAYER_COLOR
    });
    divLayer.appendChild(divFloat);
    parentBody.appendChild(divLayer);
  }
  else {
    parentBody.appendChild(divFloat);
  }

  if (TIME) {
    window.setTimeout(function() {
      container.style.display = 'none';
    }, TIME * 1000);
  }

  win.addEventListener("resize", handleSizes);
  handleSizes();
  win.addEventListener("resize", handlePosition);
  handlePosition();

  function handleSizes() {
    var
    elementContent = IS_IMAGE_FORMAT?imageContent:iframeContent,
    maxInnerWidth = win.innerWidth,
    referenceWidth = INTERSTITIAL_WIDTH,
    referenceHeigth = INTERSTITIAL_HEIGHT;

    if (RESIZE_TO_SCREEN == 'width') {
      var
      resizeWidth = maxInnerWidth,
      resizeHeight = (resizeWidth / referenceWidth) * referenceHeigth;

      object_assign(elementContent.style, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      object_assign(elementContent, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      setTimeout(function() {
        object_assign(anchorClick.style, {
          width: utils.parse(resizeWidth),
          height: utils.parse(elementContent.clientHeight),
        });
      });
    }
    else if (RESIZE_TO_SCREEN == 'height') {
      var
      resizeHeight = win.innerHeight,
      resizeWidth = (resizeHeight / referenceHeigth) * referenceWidth;

      object_assign(elementContent.style, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      object_assign(elementContent, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      setTimeout(function() {
        object_assign(anchorClick.style, {
          width: utils.parse(elementContent.clientWidth),
          height: utils.parse(resizeHeight)
        });
      });
    }
    else if (RESIZE_TO_SCREEN == 'width_height') {
      var
      resizeWidth = maxInnerWidth,
      resizeHeight = win.innerHeight;

      object_assign(elementContent.style, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      object_assign(elementContent, {
        width: utils.parse(resizeWidth),
        height: utils.parse(resizeHeight)
      });
      setTimeout(function() {
        object_assign(anchorClick.style, {
          width: utils.parse(elementContent.clientWidth),
          height: utils.parse(resizeHeight)
        });
      });
    }
    else {
      object_assign(elementContent.style, {
        width: utils.parse(referenceWidth),
        height: utils.parse(referenceHeigth)
      });
      object_assign(elementContent, {
        width: utils.parse(referenceWidth),
        height: utils.parse(referenceHeigth)
      });
      setTimeout(function() {
        object_assign(anchorClick.style, {
          width: utils.parse(referenceWidth),
          height: utils.parse(referenceHeigth)
        });
      });
    }
  }

  function handlePosition() {
    var
    topPosition = utils.parse((win.innerWidth / 2) - (divFloat.clientWidth / 2)),
    leftPosition = utils.parse((win.innerHeight / 2) - (divFloat.clientHeight / 2));

    if (RESIZE_TO_SCREEN == 'width_height') {
      object_assign(divFloat.style, {top: utils.parse(0), left: utils.parse(0)});
    }
    else if (POSITION_AS == 'center') {
      object_assign(divFloat.style, {top: leftPosition, left: topPosition});
    }
    else if (POSITION_AS == 'top_left') {
      object_assign(divFloat.style, {top: utils.parse(0), left: utils.parse(0)});
    }
    else if (POSITION_AS == 'top_center') {
      object_assign(divFloat.style, {left: topPosition});
    }
    else if (POSITION_AS == 'top_right') {
      object_assign(divFloat.style, {top: utils.parse(0), right: utils.parse(0)});
    }
    else if (POSITION_AS == 'right_center') {
      object_assign(divFloat.style, {right: utils.parse(0), top: leftPosition});
    }
    else if (POSITION_AS == 'bottom_right') {
      object_assign(divFloat.style, {bottom: utils.parse(0), right: utils.parse(0)});
    }
    else if (POSITION_AS == 'bottom_center') {
      object_assign(divFloat.style, {bottom: utils.parse(0), left: topPosition});
    }
    else if (POSITION_AS == 'bottom_left') {
      object_assign(divFloat.style, {bottom: utils.parse(0), left: utils.parse(0)});
    }
    else if (POSITION_AS == 'left_center') {
      object_assign(divFloat.style, {left: utils.parse(0), top: leftPosition});
    }
    IS_DEBUG && console.log('TEMPLATE LOG: ', 'position_as', POSITION_AS, {
      top: divFloat.style.top,
      right: divFloat.style.right,
      bottom: divFloat.style.bottom,
      left: divFloat.style.left,
      divFloat: {width: divFloat.clientWidth, height: divFloat.clientHeight}
    });
  }

  if (!IS_IMAGE_FORMAT) {
    var eventNames = [
      'mouseenter',
      'mouseout',
      'mouseover',
      'mouseleave',
      'click'
    ];
    var iframes = [iframeContent];
    for (var indexElement = 0; indexElement < iframes.length; indexElement++) {
      var element = iframes[indexElement].parentElement;
      for (var indexEvent = 0; indexEvent < eventNames.length; indexEvent++) {
        var eventName = eventNames[indexEvent];
        element.addEventListener(eventName, function(event) {
          for (var indexFrameElement = 0; indexFrameElement < iframes.length; indexFrameElement++) {
            var
            iframe = iframes[indexFrameElement],
            data = {
              action: 'adnx.template.event.'+event.type
            };
            IS_DEBUG && console.log('TEMPLATE LOG: ', 'postMessage', {iframe: iframe, data: data});
            iframe && iframe.contentWindow.postMessage(data, '*');
          }
        });
      }
    }
  }

  function object_assign() {
    var obj = arguments[0];
    for(var index = 1; index < arguments.length; index++) {
      var args = arguments[index];
      for (field in args) {
        obj[field] = args[field];
      }
    }
    return obj;
  }

})();
