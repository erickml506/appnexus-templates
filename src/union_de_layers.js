(function () {
  var
    CACHEBUSTER = '${CACHEBUSTER}'

  var CREATIVE_ID = '${CREATIVE_ID}'

  var MEDIA_URL = '${MEDIA_URL}'

  var CLICK_URL = '${CLICK_URL}'

  var URL_LANDING_REDIRECT = '#{URL_LANDING_REDIRECT}'

  var TYPE_INTERACTION = '#{TYPE_INTERACTION}'

  var MEDIA_ZOCALO_LEFT = '#{MEDIA_ZOCALO_LEFT}'

  var MEDIA_ZOCALO_RIGHT = '#{MEDIA_ZOCALO_RIGHT}'

  var EXTENSION_TYPE_ZOCALOS = '#{EXTENSION_TYPE_ZOCALOS}'

  var EXTENSION_TYPE_LAYER = '#{EXTENSION_TYPE_LAYER}'

  var MEDIA_LAYER = '#{MEDIA_LAYER}'

  var MEDIA_LAYER_SIZE = '#{MEDIA_LAYER_SIZE}'

  var ICON_CLOSE_LAYER = '#{ICON_CLOSE_LAYER}'

  var TIME_EFFECT = '#{TIME_EFFECT}'

 

  /*
    |__________________
    |  HELPERS
    |
    */
  const makeUrlRedirect = function (url, urlredir) {
    urlredir = url.split('clickenc=')[0] + 'clickenc=' + urlredir
    return urlredir
  }
  const getWidthOrHeight = function (size, wh) {
    return size.toLocaleLowerCase()
      .split('x')[wh === 'width' ? 0 : 1] + 'px'
  }
  const object_assign = function () {
    var obj = arguments[0]
    var field
    for (var index = 1; index < arguments.length; index++) {
      var args = arguments[index]
      for (field in args) {
        obj[field] = args[field]
      }
    }
    return obj
  }
  const animateZocalos = function (zocalo_1, zocalo_2, i, animateParents = true) {
    if (animateParents) {
      zocalo_1.parentElement.style.left = i === 0 ? '-152px' : '0'
      zocalo_2.parentElement.style.left = i === 0 ? '0' : '-152px'
    } else {
      zocalo_1.style.left = '-152px'
      zocalo_2.style.left = '0'
    }
  }
  const setAnchorTo = function (divToappend, anchor_click) {
    divToappend.appendChild(anchor_click)
    anchor_click.addEventListener('click', function () {
      window.open(makeUrlRedirect(CLICK_URL, URL_LANDING_REDIRECT), '_blank')
    })
  }
  const whichTransitionEvent = function () {
    var t

    var el = document.createElement('fake')

    var transitions = {
      'transition': 'transitionend',
      'OTransition': 'oTransitionEnd',
      'MozTransition': 'transitionend',
      'WebkitTransition': 'webkitTransitionEnd'
    }

    for (t in transitions) {
      if (el.style[t] !== undefined) {
        return transitions[t]
      }
    }
  }
  function getTransitionProperty () {
    var element = document.createElement('fake')
    var properties = ['transition', 'WebkitTransition',
      'MozTransition', 'msTransition',
      'OTransition']
    var p
    while (p = properties.shift()) {
      if (element.style[p] !== undefined) {
        return p
      }
    }
    return false
  }
  const makeMediaTag = function (EXTENSION_TYPE, URL_SRC) {
    var mediaTag
    switch (EXTENSION_TYPE) {
      case 'image/gif/png':
        mediaTag = document.createElement('img')
        mediaTag.src = URL_SRC + '?' + CACHEBUSTER
        break
      case 'HTML':
        mediaTag = document.createElement('iframe')
        mediaTag.src = URL_SRC + '?' + CACHEBUSTER
        mediaTag.width = '100%'
        mediaTag.height = '100%'
        mediaTag.scrolling = 'no'
        mediaTag.frameborder = 'no'
        mediaTag.style.border = '0'
        break
      default:
        console.log('The template' + CREATIVE_ID + 'doesnt suppot this format')
    }
    return mediaTag
  }

  const setEvents = function (TYPE_INTERACTION, TIME_EFFECT){
    switch (TYPE_INTERACTION) {
        case 'click':
        case 'mouseover':
          [tag_zocalo_left, tag_zocalo_right].forEach(function (el, i) {
            var element = i === 0 ? tag_zocalo_right : tag_zocalo_left
            el.addEventListener(TYPE_INTERACTION, function () {
              animateZocalos(this, element, i)
              console.log('anmimatesd')
            })
          })
          break
        case 'time':
          var time = TIME_EFFECT * 1000
          setTimeout(function () {
            animateZocalos(div_zocalo_left, div_zocalo_right, false, false)
          }, time)
          break
        default:
          console.log('try with other effect')
      }
  }

  /*
    |__________________
    |  TAGS AND VARS
    |
    */

  var parentBody = document.body.ownerDocument.defaultView.parent.document.body

  var div_parent = document.createElement('div')

  var div_parent_zocalos = document.createElement('div')

  var div_zocalo_right = document.createElement('div')

  var div_zocalo_left = document.createElement('div')

  var div_parent_layer = document.createElement('div')

  var tag_zocalo_left = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_LEFT)

  var tag_zocalo_right = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_ZOCALO_RIGHT)

  var tag_layer = makeMediaTag(EXTENSION_TYPE_ZOCALOS, MEDIA_LAYER)

  var layer_anchor_click = document.createElement('a')

  var div_close_layer = document.createElement('div')

  /*
    |__________________
    |  SET STYLES
    |
    */
  parentBody.classList.add('tc')
  object_assign(div_parent.style, {
    position: 'fixed',
    width: '100%',
    height: '0px',
    zIndex: '999999999999',
    display: 'block'
  })
  object_assign(div_parent_zocalos.style, {
    position: 'relative',
    width: '1px',
    height: '1px',
    margin: '0 auto'
  })
  var transitionCompatibility = getTransitionProperty()
  object_assign(div_zocalo_left.style, {
    position: 'absolute',
    top: '0',
    left: '-662px',
    width: '150px',
    height: '600px',
    display: 'block',
    zIndex: '998'
  })
  div_zocalo_left.style[transitionCompatibility] = 'left 2s'

  object_assign(div_zocalo_right.style, {
    position: 'absolute',
    top: '0',
    left: '512px',
    width: '150px',
    height: '600px',
    display: 'block',
    zIndex: '998'
  })
  div_zocalo_right.style[transitionCompatibility] = 'left 2s'

  object_assign(div_parent_layer.style, {
    width: getWidthOrHeight(MEDIA_LAYER_SIZE, 'width'),
    height: getWidthOrHeight(MEDIA_LAYER_SIZE, 'height'),
    position: 'fixed',
    top: '0',
    left: '0',
    right: '0',
    margin: '0 auto',
    zIndex: '999',
    cursor: 'pointer',
    display: 'none'
  })
  object_assign(tag_layer.style, {
    width: '100%',
    height: '100%'
  })

  object_assign(layer_anchor_click.style, {
    height: '100%',
    left: '0',
    position: 'absolute',
    top: '0',
    width: '100%',
    zIndex: '1',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    cursor: 'pointer'
  })
  object_assign(div_close_layer.style, {
    position: 'absolute',
    top: '0',
    right: '0',
    width: '50px',
    height: '50px',
    zIndex: '99',
    cursor: 'pointer',
    backgroundSize: 'cover',
    backgroundImage: 'url(' + ICON_CLOSE_LAYER + ')'
  })

  setEvents(TYPE_INTERACTION, TIME_EFFECT);

  var transitionEvent = whichTransitionEvent()
  div_zocalo_left.addEventListener(transitionEvent, function () {
    div_zocalo_left.style.display = 'none'
    div_zocalo_right.style.display = 'none'
    div_parent_layer.style.display = 'block'
  }, false)

  div_close_layer.addEventListener('click', function () {
    div_parent_layer.style.display = 'none'
  })
  /*
    |__________________
    |  APPEND CHILDS
    |
    */
  setAnchorTo(div_parent_layer, layer_anchor_click)
  div_parent_layer.appendChild(tag_layer)
  div_parent_layer.appendChild(div_close_layer)
  div_zocalo_left.appendChild(tag_zocalo_left)
  div_parent_zocalos.appendChild(div_zocalo_left)
  div_zocalo_right.appendChild(tag_zocalo_right)
  div_parent_zocalos.appendChild(div_zocalo_right)
  div_parent.appendChild(div_parent_zocalos)
  div_parent.appendChild(div_parent_layer)
  parentBody.insertBefore(div_parent, parentBody.childNodes[0])
  if ((location.href).search('debug')>=0) console.log(MEDIA_URL, EXTENSION_TYPE_LAYER)
})()
