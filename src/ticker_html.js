(function () {
    var CACHEBUSTER = '${CACHEBUSTER}'
    var MEDIA_URL = '${MEDIA_URL}'
    var CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}'
    var CREATIVE_WIDTH = '${CREATIVE_WIDTH}'
    var CLICK_URL = '${CLICK_URL}'
    var LANDING_URL = '#{CLICK_URL}'
    const makeUrlRedirect = function(url, urlredir){
        urlredir = url.split("clickenc=")[0] + 'clickenc=' + urlredir;
        return urlredir;
    }
    var utils = {
      parsePixel: function (value) {
        return (isNaN(value)) ? value : (value + 'px')
      },
      parsePercentage: function (value) {
        return (isNaN(value)) ? value : (value + '%')
      }
    }
  
    var divFloat = document.createElement('div')
    var anchorClick = document.createElement('a')
    var iframeWrapper = document.createElement('div')
    var iframe = document.createElement('iframe')
    container = divFloat
  
    object_assign(divFloat.style, {
      position: 'relative',
      width: '100%'
    })
  
    anchorClick.href = makeUrlRedirect(CLICK_URL,LANDING_URL);
    anchorClick.target = '_blank'
  
    object_assign(anchorClick.style, {
      curso: 'pointer',
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      zIndex: '1'
    })
  
    var ratioIframe = CREATIVE_HEIGHT * 100 / CREATIVE_WIDTH
  
    object_assign(iframeWrapper.style, {
      position: 'relative',
      paddingBottom: '45px',
      height: '0'
    })
  
    iframe.src = MEDIA_URL  
    iframe.scrolling = 'no'
    object_assign(iframe.style, {
      border: 0,
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%'
    })
  
    iframeWrapper.appendChild(iframe)
    iframeWrapper.appendChild(anchorClick)
    divFloat.appendChild(iframeWrapper)
  
    document.body.appendChild(divFloat)
  
    var apnTagTargetId = document.body.ownerDocument.defaultView.apntag_targetId
    var divApn = document.body.ownerDocument.defaultView.parent.document.getElementById('div_utif_' + apnTagTargetId)
    var iframeApn = divApn.querySelector('iframe')
  
    object_assign(iframeApn, {
      height: '45',
      width: '100%'
    })
  
    var template = {
      container: divFloat,
      params: {
        height: CREATIVE_HEIGHT,
        width: CREATIVE_WIDTH
      },
      handlePosition: function () {
        if (!this.container.ownerDocument) return
        var win = this.container.ownerDocument.defaultView.parent
      }
    }
  
    divFloat.ownerDocument.defaultView.parent.addEventListener("resize", template.handlePosition.bind(template))
    template.handlePosition()
  
    function object_assign () {
      var obj = arguments[0]
      for (var index = 1; index < arguments.length; index++) {
        var args = arguments[index]
        for (field in args) {
          obj[field] = args[field]
        }
      }
      return obj
    }
  })()