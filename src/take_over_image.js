
(function () {
    var CLICK_URL = '${CLICK_URL}'
    var MEDIA_URL = '${MEDIA_URL}'
    var CREATIVE_WIDTH = '${CREATIVE_WIDTH}'
    var CREATIVE_HEIGHT = '${CREATIVE_HEIGHT}'
    var CACHEBUSTER = '${CACHEBUSTER}'
  
    var IMAGE_TAKEOVER = '#{IMAGE_TAKEOVER}'
    var IMAGE_TAKEOVER_HEIGHT = parseInt('#{IMAGE_TAKEOVER_HEIGHT}')
    var IMAGE_TAKEOVER_WIDTH = parseInt('#{IMAGE_TAKEOVER_WIDTH}')
  
    var BUTTON_COLLAPSE_WIDTH = parseInt('#{BUTTON_COLLAPSE_WIDTH}')
    var BUTTON_COLLAPSE_HEIGHT = parseInt('#{BUTTON_COLLAPSE_HEIGHT}')
    var BUTTON_COLLAPSE_RIGHT = parseInt('#{BUTTON_COLLAPSE_RIGHT}')
    var BUTTON_COLLAPSE_TOP = parseInt('#{BUTTON_COLLAPSE_TOP}')
  
    var BUTTON_EXPAND_WIDTH = parseInt('#{BUTTON_EXPAND_WIDTH}')
    var BUTTON_EXPAND_HEIGHT = parseInt('#{BUTTON_EXPAND_HEIGHT}')
    var BUTTON_EXPAND_RIGHT = parseInt('#{BUTTON_EXPAND_RIGHT}')
    var BUTTON_EXPAND_TOP = parseInt('#{BUTTON_EXPAND_TOP}')
    
    var TIME = parseInt('#{TIME}')
  
    const
      rd = CACHEBUSTER,
      utils = {
        parse: function (value) {
          return (isNaN(value)) ? value : (value + "px")
        }
      }
  
    var idAnimationTimeout = null
  
    var executingAnimation = false
    var screenWidth = window.parent.innerWidth
    var screenHeight = window.parent.innerHeight
    var currentLeft = screenWidth
    var finalEnter = 0
    var finalExit = screenWidth
    var firstTime = true
  
    var parentBody = document.body.ownerDocument.defaultView.parent.document.body
    
    var divBanner = document.createElement('div')
    var divButtonExpand = document.createElement('div')
    var anchorBannerClick = document.createElement('a')
    var imgBannerContent = document.createElement('img')
    
    var divLayer = document.createElement('div')
    var divTakeOver = document.createElement('div')
    var divButtonCollapse = document.createElement('div')
    var anchorTakeOverClick = document.createElement('a')
    var imgTakeOverContent = document.createElement('img')
      
    object_assign(divLayer.style, {
      backgroundColor: 'blank',
      height: '100%',
      left: utils.parse(finalExit),
      position: 'absolute',
      textAlign: 'left',
      top: '0px',
      verticalAlign: 'middle',
      visibility: 'visible',
      width: '100%',
      zIndex: '99999999999'
    })
  
    var win = divLayer.ownerDocument.defaultView.parent
    object_assign(divTakeOver.style, {
      height: utils.parse(IMAGE_TAKEOVER_HEIGHT),
      left: utils.parse((win.innerWidth / 2) - (IMAGE_TAKEOVER_WIDTH / 2)),
      position: 'absolute',
      top: utils.parse((win.innerHeight / 2) - (IMAGE_TAKEOVER_HEIGHT / 2)),
      width: utils.parse(IMAGE_TAKEOVER_WIDTH)
    })
  
    object_assign(divButtonCollapse.style, {
      cursor: 'pointer',
      height: utils.parse(BUTTON_COLLAPSE_HEIGHT),
      position: 'absolute',
      right: utils.parse(BUTTON_COLLAPSE_RIGHT),
      top: utils.parse(BUTTON_COLLAPSE_TOP),
      width: utils.parse(BUTTON_COLLAPSE_WIDTH)
    })
  
    object_assign(divButtonExpand.style, {
      cursor: 'pointer',
      height: utils.parse(BUTTON_EXPAND_HEIGHT),
      position: 'absolute',
      right: utils.parse(BUTTON_EXPAND_RIGHT),
      top: utils.parse(BUTTON_EXPAND_TOP),
      width: utils.parse(BUTTON_EXPAND_WIDTH)
    })
  
    document.body.appendChild(divBanner)
    divBanner.appendChild(divButtonExpand)
    divBanner.appendChild(anchorBannerClick)
    anchorBannerClick.appendChild(imgBannerContent)
    anchorBannerClick.onclick = function () {
      window.open(CLICK_URL)
    }
    imgBannerContent.src = MEDIA_URL + '?' + CACHEBUSTER
    object_assign(imgBannerContent.style, {
      height: utils.parse(CREATIVE_HEIGHT),
      width: utils.parse(CREATIVE_WIDTH)
    })
    object_assign(divBanner.style, {
      height: utils.parse(CREATIVE_HEIGHT),
      position: 'absolute',
      width: utils.parse(CREATIVE_WIDTH)
    })
    if ('#{BUTTON_EXPAND}' && '#{BUTTON_EXPAND}' != '\#\{BUTTON_EXPAND\}') {
      var imgButtonExpand = document.createElement('img')
      imgButtonExpand.src = '#{BUTTON_EXPAND}'
      object_assign(imgButtonExpand.style, {
        position: 'absolute',
        right: '0px',
        top: '0px'
      })
      divButtonExpand.appendChild(imgButtonExpand)
    }
  
    divLayer.id = 'divLayer'
  
    parentBody.appendChild(divLayer)
    divLayer.appendChild(divTakeOver)
    divTakeOver.appendChild(divButtonCollapse)
    divTakeOver.appendChild(anchorTakeOverClick)
    anchorTakeOverClick.appendChild(imgTakeOverContent)
    imgTakeOverContent.onclick = function () {
      window.open(CLICK_URL)
    }
    imgTakeOverContent.src = IMAGE_TAKEOVER
    object_assign(imgTakeOverContent.style, {
      height: utils.parse(IMAGE_TAKEOVER_HEIGHT),
      width: utils.parse(IMAGE_TAKEOVER_WIDTH)
    })
  
    
  
    if ('#{BUTTON_COLLAPSE}' && '#{BUTTON_COLLAPSE}' != '\#\{BUTTON_COLLAPSE\}') {
      var imgButtonCollapse = document.createElement('img')
      imgButtonCollapse.src = '#{BUTTON_COLLAPSE}'
      object_assign(imgButtonCollapse.style, {
        position: 'absolute',
        right: '0px',
        top: '0px',
        width: '100%'
      })
      divButtonCollapse.appendChild(imgButtonCollapse)
    }
  
 
    function expand() {
      object_assign(parentBody.style, {
        overflow: 'hidden'
      })
      object_assign(divLayer.style, {
        visibility: 'visible',
        display: 'block'
      })
      setTimeout(function () {
        if (firstTime) {
          window.parent.scroll(currentLeft, 0)
          currentLeft = 0
          firstTime = false
        } else {
          enderTakeOver()
        }
      }, 20)
      idAnimationTimeout = window.setTimeout(collapse, TIME * 1000)
    }
    divButtonExpand.onclick = expand
  
    function collapse() {
      idAnimationTimeout && clearTimeout(idAnimationTimeout)
      object_assign(parentBody.style, {
        overflow: null,
      })
      setTimeout(function () {
        exitTakeOver(function () {
          object_assign(divLayer.style, {
            display: 'none',
            visibility: 'hidden'
          })
        })
      }, 20)
    }
    divButtonCollapse.onclick = collapse
  
    function enderTakeOver(cb) {
      var done = false
      var newLeft = currentLeft - 30
  
      if (newLeft <= finalEnter) {
        newLeft = finalEnter
        done = true
      }
      currentLeft = newLeft
      window.parent.scroll(finalExit - currentLeft, 0)
      if (!done) {
        setTimeout(function () {
          enderTakeOver(cb)
        }, 15)
      } else {
        cb && cb()
      }
    }
  
    function exitTakeOver(cb) {
      var done = false
      var newLeft = currentLeft + 30
  
      if (newLeft >= finalExit) {
        newLeft = finalExit
        done = true
      }
      currentLeft = newLeft
      window.parent.scroll(finalExit - currentLeft, 0)
      if (!done) {
        setTimeout(function () {
          exitTakeOver(cb)
        }, 15)
      } else {
        cb && cb()
      }
    }
  
    if (TIME) {
      expand()
    } else {
      collapse()
    }
  
    function object_assign() {
      var obj = arguments[0]
      for (var index = 1; index < arguments.length; index++) {
        var args = arguments[index]
        for (field in args) {
          obj[field] = args[field]
        }
      }
      return obj
    }
  })()
  