const path = require('path')

module.exports = {
    mode: 'production',
    entry: {
        app: './src/take_over_fixed_html.js'
    },
    output: {
        path: path.join(__dirname, 'builded'),
        filename: 'take_over_fixed_html-compiled.js'
    },
    optimization: {
        minimize: true
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: ["env"]
            },
        }]
    }
}